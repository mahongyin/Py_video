import os
from importlib import import_module
from flask import Flask, render_template, Response

if os.environ.get('CAMERA'):
    Camera = import_module('camera_' + os.environ['CAMERA'])#.Camera
else:
    from camera_pil import Camera

app = Flask(__name__)


@app.route('/')
def index():
    """
    视图函数
    :return:
    """
    return render_template('index.html')


def gen(camera):
    """
    流媒体发生器
    """
    while True:
        frame = camera.get_frame()

        yield (b'--frame\r\n'
               b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')


@app.route('/video_feed')
def video_feed():
    """流媒体数据"""
    return Response(gen(Camera()),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


if __name__ == '__main__':
    app.run(threaded=True, host="127.0.0.1", port=80)
    # 若是要使用的话, 请把 host=“127.0.0.1” 改成 host=“0.0.0.0” 而后运行屏幕共享服务器

import tkinter
from tkinter import scrolledtext
from tkinter import Entry, Button
from socket import *
from time import ctime, sleep

global tcpCliSock
global udpCliSock


def main():
    BUFSIZ = 1024
    window = tkinter.Tk()
    window.geometry('480x540')  # 大小
    # window.geometry(f'{width}x{height}')
    window.title("TCP&UDP调试工具")
    # 控制是否正在发送的变量
    # 连接
    connect = tkinter.BooleanVar(window, value=False)
    # 收发
    revering = tkinter.BooleanVar(window, value=False)
    v = tkinter.IntVar()
    rad1 = tkinter.Radiobutton(window, text='TCP', value=1, variable=v)
    rad2 = tkinter.Radiobutton(window, text='UDP', value=2, variable=v)
    v.set(1)
    rad1.grid(column=0, row=0)
    rad2.grid(column=1, row=0)
    tkinter.Label(window, text="客户端模式", fg='red').grid(row=1, column=0)
    tkinter.Label(window, text="目标IP").grid(row=2)
    ip = tkinter.StringVar()
    ip.set('127.0.0.1')
    tkinter.Entry(window, textvariable=ip).grid(row=2, column=1)

    tkinter.Label(window, text="目标Port").grid(row=3)
    port = tkinter.IntVar()
    port.set(7878)
    tkinter.Entry(window, textvariable=port).grid(row=3, column=1)

    tcpCliSock = socket(AF_INET, SOCK_STREAM)  ##创建客户端TCP套接字
    udpCliSock = socket(AF_INET, SOCK_DGRAM)  ##UDP

    def click_cb00():
        address = (ip.get(), port.get())
        if v.get() == 1:  # TCP
            if connect.get():
                print('断开连接')
                tcpCliSock.close()
                connect.set(False)
                btnStart0['text'] = "连接"
                return
            else:
                print('连接')
                tcpCliSock.connect(address)  ##连接服务器
                btnStart0['text'] = "断开"
                # btnStart0['state'] = 'disabled'
                connect.set(True)
        elif v.get() == 2:  # UDP
            if connect.get():
                udpCliSock.close()
                btnStart0['text'] = "连接"
                connect.set(False)
            else:
                btnStart0['text'] = "断开"
                connect.set(True)

    btnStart0 = Button(window, text="连接", command=click_cb00, bg='white', activeforeground='red')
    btnStart0.grid(row=4, column=1)

    tkinter.Label(window, text="服务器模式", fg='red').grid(row=5, column=0)
    tkinter.Label(window, text="监听本机端口").grid(row=6, column=0)
    port2 = tkinter.IntVar()
    port2.set(8090)
    Entry(window, width=10, textvariable=port2).grid(column=1, row=6)

    def click_cb0():
        if v.get() == 1:  # TCP
            HOST = '0.0.0.0'
            ADDR = (HOST, port2.get())
            if revering.get():
                tcpCliSock.close()
                btnStart['text'] = "监听"
                revering.set(False)
            else:
                # tcpCliSock = socket(AF_INET, SOCK_STREAM)  ##创建服务器TCP套接字
                tcpCliSock.bind(ADDR)
                tcpCliSock.listen(5)
                print('监听')
                btnStart['text'] = "断开"
                # btnStart['state'] = 'disabled'
                revering.set(True)
                while connect.get():
                    data = tcpCliSock.recv(BUFSIZ)  ##监听客户端是否发送消息
                    if not data:
                        break
                    scro_txt.insert(tkinter.END, data.decode())
                    print(data)
            # while True:
            #     print('waiting for connection...')
            #     tcpCliSock, addr = tcpSerSock.accept()  ##等待客户端连接
            #     print('...connected from:', addr)
            # tcpSerSock.close()
        elif v.get() == 2:  # UDP
            HOST = '0.0.0.0'
            ADDR = (HOST, port2.get())
            if revering.get():
                udpCliSock.close()
                btnStart['text'] = "监听"
                revering.set(False)
            else:
                # udpSerSock = socket(AF_INET, SOCK_DGRAM)
                udpCliSock.bind(ADDR)
                btnStart['text'] = "断开"
                revering.set(True)
                print('监听')
                while connect.get():
                    print('waiting for message...')
                    data, addr = udpCliSock.recvfrom(BUFSIZ)  ##监听并接受客户端发的消息
                    if not data:
                        break
                    print(data.decode())
                    scro_txt.insert(tkinter.END, data.decode())
                    print('...received from and returned to:', addr)

    btnStart = Button(window, text="监听", command=click_cb0, bg='white', activeforeground='red')
    btnStart.grid(row=6, column=2)

    tkinter.Label(window, text="接收区", fg='red').grid(row=7, column=0)
    tkinter.Checkbutton(window, text="16进制").grid(columnspan=2, row=7)

    def click_cb():
        scro_txt.delete(1.0, tkinter.END)

    # command=lambda : click_cb(text_var),
    Button(window, text="清空", command=click_cb, bg='white', state='normal',
           activeforeground='red', activebackground='yellow').grid(row=7, column=2)

    scro_txt = scrolledtext.ScrolledText(window, width=40, height=10)
    scro_txt.grid(column=1, row=8)
    # scro_txt.insert(1.0, 'abc')  #插入第一条
    # scro_txt.insert(tkinter.END, '123')  #从末尾添加

    tkinter.Label(window, text="发送区", fg='red').grid(row=10, column=0)
    tkinter.Checkbutton(window, text="16进制").grid(columnspan=2, row=10)

    def click_cb2():
        scro_txt2.delete(1.0, tkinter.END)

    Button(window, text="清空", command=click_cb2, bg='white', state='normal',
           activeforeground='red', activebackground='yellow').grid(row=10, column=2)

    scro_txt2 = scrolledtext.ScrolledText(window, width=40, height=10)
    scro_txt2.grid(column=1, row=13)

    def click_cb3():
        if connect.get():
            print('发送')
            data = scro_txt2.get(1.0, tkinter.END).encode()
            if v.get() == 1:  # TCP
                if not data:
                    return
                # tcpCliSock.send('[%s] %s' % (ctime(), data))
                tcpCliSock.send(data)  ##给服务器发送数据
            elif v.get() == 2:  # UDP
                if not data:
                    return
                address = (ip.get(), port.get())
                # udpCliSock.sendto('[%s]' % (ctime(), data))  ##给消息加上时间戳并返回给客户端
                udpCliSock.sendto(data, address)  ##给服务器发送数据
        else:
            print('not connected')

    Button(window, text="发送", command=click_cb3, bg='white', activeforeground='red').grid(row=14, column=0)

    window.geometry('+500+200')  # 位置
    window.mainloop()


# 广播发送端
def broadcast():
    cast = ("192.168.32.255", 9998)
    # 创建数据包套接字
    s = socket(AF_INET, SOCK_DGRAM)
    # 设置可以广播
    s.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
    data = "十里平湖霜满天，寸寸青丝愁华年。对月形单望相互，只羡鸳鸯不羡仙。"
    while True:
        sleep(2)
        # 给广播地址发送信息,目标地址等于广播地址
        s.sendto(data.encode(), cast)


#  广播接收端
def recever():
    # 创建upd套接字
    soc = socket(AF_INET, SOCK_DGRAM)
    # 设置套接字可以接收广播
    soc.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
    # 绑定地址,设置接收广播的端口号
    soc.bind(("0.0.0.0", 9998))
    while True:
        buffer_byte, addr = soc.recvfrom(1024)
        print(buffer_byte.decode())


if __name__ == '__main__':
    main()

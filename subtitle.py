from os.path import splitext, isfile

from moviepy.editor import (VideoFileClip, TextClip, CompositeVideoClip)

# 字幕文件  格式->   字幕内容 : 开始时间10: 持续时间5
class RealizeAddSubtitles():
    '''
    合成字幕与视频
    '''

    def __init__(self, videoFile, txtFile):

        self.src_video = videoFile
        self.sentences = txtFile
        # src_video = input('请输入视频文件路径')
        # sentences = input('请输入字幕文件路径')
        if not (isfile(self.src_video) and self.src_video.endswith(('.avi', '.mp4')) and isfile(
                self.sentences) and self.sentences.endswith('.txt')):
            print('视频仅支持avi以及mp4，字幕仅支持txt格式')
        else:
            video = VideoFileClip(self.src_video)
            # 获取视频的宽度和高度
            w, h = video.w, video.h
            # 所有字幕剪辑
            txts = []
            with open(self.sentences, encoding='utf-8') as fp:
                for line in fp:
                    sentences, start, span = line.split(': ')
                    start, span = map(float, (start, span))
                    txt = (TextClip(sentences, fontsize=40, font='SimHei', size=(w - 20, 40), align='center', color='white')
                       .set_position((10, h - 150))
                       .set_duration(span).set_start(start))
                    txts.append(txt)
            # 合成视频，写入文件
            video = CompositeVideoClip([video, *txts])
            fn, ext = splitext(self.src_video)
            video.write_videofile(f'{fn}_带字幕{ext}')


if __name__ == '__main__':
    '''调用方法示例'''
    addSubtitles = RealizeAddSubtitles('D:/Video/Fong.mp4', 'D:/Video/Love.txt')

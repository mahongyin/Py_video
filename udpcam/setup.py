from cx_Freeze import setup, Executable
# 打包1
# pyinstaller -F server.py -w   //无cmd

# 打包exe命令：（具体的命令网上资料很多）
# # 打包1个py文件，并隐藏执行窗口
# pyinstaller - F - w
# main.py
# # 打包1个py文件(-F)，并隐藏执行窗口(-w),替换exe的ico图标(-i D:/img.ico)
# pyinstaller - F - w - i D:/img.ico main.py

# 以上的这种打包方式会将各种依赖库都以源文件方式保存到文件夹中，大部分时候我们还是希望只有一个exe文件
# 将文件夹中所有依赖库都打包进exe内：
# 合并到一个exe内（--onefile）,替换exe图标(--icon=img.ico)，py源文件(main.py)，隐藏执行(-w)
# pyinstaller - -onefile - -icon = img.ico main.py - w
# 注意：当把所有依赖库都打包进一个exe以后，且以隐藏CMD窗口方式执行时会出现错误，导致程序无法正常运行，所以需要用到
# subprocess来执行CMD命令。这种方式来执行cmd命令就不会出现程序错误。
# import subprocess
# cmd = '你的CMD命令'
# res = subprocess.call(cmd, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)


#打包1
#nuitka --mingw64 --standalone --show-progress --show-memory --plugin-enable=tk-inter --plugin-enable=numpy --windows-company-name=中视广信 --windows-product -name=屏幕广播 --windows-file-version=1000 --windows-product-version=1000 --windows-file-description=Windows下软件的作用描述 --output-dir=out server.py

# 打包2
#setup.py
# 3.执行 python setup.py build
# 4.在build文件夹下生成所要的exe文件
executables = [Executable(script='server.py', targetName='server.exe', )]
setup(name='屏幕广播', version='0.1', description='广播服务', executables=executables)

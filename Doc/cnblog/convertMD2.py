# coding:utf-8
# 引入 requests库
import requests
# 从bs4库中引入BeautifulSoup
from bs4 import BeautifulSoup
# 引入正则表达式依赖
import re
# 引入文件相关依赖
import os

# 全局变量，主要是为了生成序号，方法内部最后一个赋值很重要，没有这个赋值，会导致序号不连续
global index


# 定义了一个方法，这个方法就是为了循环遍历我的博客，文件就是我们要保存的文件
def get_blog_list(user, page, index, file):
    # 通过requests发送请求，这里用到了字符串的格式化
    # r = requests.get('https://www.cnblogs.com/caoleiCoding/default.html?page={}'.format(page))
    r = requests.get('https://www.cnblogs.com/' + user + '/default.html?page={}'.format(page))
    # 打印返回结果
    print(r.text)
    # 用soup库解析html，如果没有安装lxml库，会报错
    soup = BeautifulSoup(r.text, 'lxml')

    # 搜索class属性为postTitle的div
    for tag in soup.find_all('div', class_='postTitle'):
        print(tag)
        # tag的span标签的内容
        content = tag.span.contents
        # 过滤掉置顶内容，这块的结构和其他内容不一样，匹配需要写正则，太复杂，所以先忽略掉
        if index == 0 and page == 1:
            index += 1
            continue
        print(content)
        # 获取span的内容，也就是文本内容
        title = next(tag.span.children)
        print(str(title))
        # 替换其中的换行符和空格
        title = title.replace('\n', '').strip()
        print(title)
        # 获取tag下的a标签的href，也就是我们的博客地址
        url = tag.a['href']
        print(tag.a['href'])
        # 通过字符串格式化写入解析的内容
        file.write('{}. [{}]({})\n'.format(index, title, url))
        # 序号自增
        index += 1
        # 给全局变量赋值
        inedx = index


# 博客园
if __name__ == "__main__":
    # 指定博客园用户ID
    targetUser = 'mahongyin'
    index = 0
    # 定义我们的markdwon文件，模式为写，如果没有的话，会新增
    file = open(r'./md/'+targetUser+'-blog-list.md', 'w', encoding='utf-8')
    # 我现在的包括是60页，这里是循环获取
    for page in range(59):
        get_blog_list(targetUser, page + 1, index, file)

import pprint
import time
import requests
import csv

import yaml
if __name__ == '__main__':
    f = open('青灯公众号文章.csv', mode='a', encoding='utf-8', newline='')
    csv_writer = csv.DictWriter(f, fieldnames=['标题', '文章发布时间', '文章地址'])
    csv_writer.writeheader()
    with open("wechat.yaml", "r") as file:
        file_data = file.read()
    config = yaml.safe_load(file_data)
    for page in range(0, 40, 5):
        url = f'https://mp.weixin.qq.com/cgi-bin/appmsg?action=list_ex&begin={page}&count=5&fakeid=&type=9&query=&token=1252678642&lang=zh_CN&f=json&ajax=1'
        headers = {
            "Cookie": config['cookie'],
            "User-Agent": config['user_agent'],
            'referer': 'https://mp.weixin.qq.com/cgi-bin/appmsg?t=media/appmsg_edit_v2&action=edit&isNew=1&type=10&createType=0&token=1252678642&lang=zh_CN'
        }

        response = requests.get(url=url, headers=headers)
        html_data = response.json()
        pprint.pprint(response.json())
        lis = html_data['app_msg_list']
        for li in lis:
            title = li['title']
            link_url = li['link']
            update_time = li['update_time']
            timeArray = time.localtime(int(update_time))
            otherStyleTime = time.strftime("%Y-%m-%d %H:%M:%S", timeArray)
            dit = {
                '标题': title,
                '文章发布时间': otherStyleTime,
                '文章地址': link_url,
            }
            csv_writer.writerow(dit)
            print(dit)

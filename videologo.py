# -*- coding: utf-8 -*-
from moviepy.editor import *


# 视频添加水印

def convert(src, dst, start=None, end=None):
    # "src是原始视频文件名，dst是要输出的视频文件名"
    print("开始处理...")
    # t1，t2 是 要处理的视频的剪辑开始和结束时间
    if not start: start = 10
    if not end: end = 10
    # 加载原视频
    clip = VideoFileClip(src)
    # 剪辑原视频
    clip = clip.subclip(start, clip.duration - end)
    # 加载自己的logo，logo_sc.png 需要预先准备
    img_clip = ImageClip("hat/logo_sc.png")
    # 把它放到左上角，并显示20秒
    img_clip = img_clip.set_pos(('left', 'top')).set_duration(20)
    # 把这个logo 叠加到剪辑好的视频上
    clip = CompositeVideoClip([clip, img_clip])
    # 输出视频
    clip.to_videofile(dst, fps=24, remove_temp=False)


convert("E:\\PyProjects\\test.mp4", "输出的视频.mp4", 10, 20)

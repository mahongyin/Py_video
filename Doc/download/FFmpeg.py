# coding:utf-8
import os

import ffmpy


# ffmpeg -y -i 视频.mp4 -i 音频.mp4 -vcodec copy -acodec copy output.mp4
def mergeAV(inputV, inputA, output):
    import ffmpy
    ff = ffmpy.FFmpeg(
        inputs={inputV: '-y', inputA: None},
        outputs={output: ['-vcodec', 'copy',
                          '-acodec', 'copy']})
    ff.run()
    print(ff.cmd)


# ffmpeg -i input.mp4  -vcodec  libx265 -preset slow -b:v 2000k  -crf  21 -strict -2 out.mp4
def converCodec(input, output):
    ff = ffmpy.FFmpeg(
        inputs={input: None},
        outputs={output: ['-vcodec', 'libx265',
                          '-preset', 'slow', '-b:v', '2000k', '-crf', '21', '-strict', '-2']})
    ff.run()
    print(ff.cmd)


# 读取文件夹下视频转码转成mp4
def converMp4(dir_name):
    # os.listdir()方法获取文件夹名字，返回数组
    file_name_list = os.listdir(dir_name)
    for name in file_name_list:
        if name.endswith(".avi"):
            ff = ffmpy.FFmpeg(
                inputs={dir_name + "\\" + name: None},
                outputs={dir_name + "\\" + name[:-4] + ".mp4": ['-vcodec', 'libx264', '-acodec', 'copy', '-y']})
            print(ff.cmd)
            ff.run()


def deleteFile(dir_name):
    file_name_list = os.listdir(dir_name)
    for name in file_name_list:
        if name.endswith(".avi"):
            os.remove(dir_name + "\\" + name)
            print("remove"+name)


if __name__ == '__main__':
    converMp4("E:\BaiduNetdiskDownload\积书视频素材\lucene")
    # deleteFile("E:\BaiduNetdiskDownload\积书视频素材\lucene")

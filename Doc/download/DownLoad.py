# coding:utf-8
import os

import requests
from requests_toolbelt import MultipartEncoder
from tqdm import tqdm
from requests_toolbelt.multipart import encoder


# 带进度
def download(url: str, fname: str):
    print('下载：' + url)
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36'
    }
    # 用流stream的方式获取url的数据
    resp = requests.get(url, headers=headers, stream=True)
    # 拿到文件的长度，并把total初始化为0
    total = int(resp.headers.get('content-length', 0))
    # 打开当前目录的fname文件(名字你来传入)
    # 初始化tqdm，传入总数，文件名等数据，接着就是写入，更新等操作了
    with open(fname, 'wb') as file, tqdm(
            desc=fname,
            total=total,
            unit='iB',
            unit_scale=True,
            unit_divisor=1024,
    ) as bar:
        for data in resp.iter_content(chunk_size=1024):
            size = file.write(data)
            bar.update(size)


# 普通下载
def download0(url: str, video_path: str):
    video_response = requests.get(url)
    video = video_response.content
    with open(video_path, 'wb') as fwb:
        fwb.write(video)


def upload0(url: str, aheaders: {}, data: MultipartEncoder, path: str):
    aheaders['Content-Type'] = data.content_type
    aheaders[
        'User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
    res0 = requests.post(url=url, data=data, headers=aheaders)
    return res0


def progress_callback(monitor):
    progress = (monitor.bytes_read / monitor.len) * 100
    print("\r 文件上传进度：%d%%(%d/%d)"
          % (progress, monitor.bytes_read, monitor.len), end=" ")
    pass


# 带进度
def upload(url: str, aheaders: {}, edata: encoder.MultipartEncoder):
    mdata = encoder.MultipartEncoderMonitor(edata, progress_callback)
    # aheaders['Content-Type'] = mdata.content_type
    # aheaders['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
    res0 = requests.post(url=url, data=mdata, headers=aheaders)
    return res0


if __name__ == "__main__":
    # 下载文件，并传入文件名
    url = "xhttps://autoupdate.termius.com/windows/Termius.exe"
    download(url, "haha.exe")

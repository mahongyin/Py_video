# coding:utf-8
import json
import os
import re
from itertools import product

import requests
from PIL import Image
from requests_toolbelt import MultipartEncoder


# contentId:false
# suffix  .png
def uploadimg(cookie, xtoken, img_path):
    url1 = 'http://jeacher.com/content/idea/upload'
    # from_data上传文件，注意参数名uploadFile
    filename, suffix = os.path.splitext(img_path)
    fname = str(filename).split("/").pop() + suffix
    opf = open(img_path, 'rb')
    data = MultipartEncoder(
        fields={'suffix': suffix, 'contentId': 'false', 'uploadFile': (fname, opf, 'image/*')})
    # fields={'suffix': suffix, 'uploadFile': ('filename', open(img_path, 'rb'), 'image/*')})
    aheaders = {'Content-Type': data.content_type,
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
                'Cookie': cookie,
                'x-token': xtoken,
                'Referer': 'http://jeacher.com'}
    res0 = requests.post(url=url1, data=data, headers=aheaders)
    opf.close()
    return res0


def download_img(title, img_url):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36'
    }
    r = requests.get(img_url, headers=headers, stream=True)
    # print(r.status_code) # 返回状态码
    fileName = ''
    if r.status_code == 200:
        if not os.path.isdir("./img/" + title):
            os.makedirs("./img/" + title)
        # 截取图片文件名
        img_name = img_url.split('#')[0].split('?')[0].split('/').pop()
        if '.png' in img_name or '.jpg' in img_name or '.jpeg' in img_name or '.gif' in img_name or '.webp' in img_name:
            fileName = "./img/" + title + "/" + img_name
            with open(fileName, 'wb') as f:
                f.write(r.content)
            print("download img", fileName)
            # 去水印
            clearWater2(fileName)
        return fileName
    else:
        return fileName


def clearWater2(imgPath):
    img = Image.open(imgPath)
    width, height = img.size
    for pos in product(range(width), range(height)):
        if sum(img.getpixel(pos)[:3]) > 600:
            img.putpixel(pos, (255, 255, 255))
    img.save(imgPath)


def upload(cookie, xtoken, title, img):
    title = re.sub('[^\w\-_\. ]', '', title)
    # print(title)
    name = download_img(title, img)
    if len(name) > 0:
        # t = time.time()
        # timestamp = int(round(t * 1000))  # 13位
        res = uploadimg(cookie, xtoken, name)
        # 'application/octet-stream'
        if res.status_code == 200:
            print('上传成功' if res.text == '' else eval("u" + "\'" + res.text + "\'"))
            return json.loads(res.text)['data']
        print('上传失败' if res.text == '' else eval("u" + "\'" + res.text + "\'"))
        return ""
    else:
        return ""


def putBlogs(cookie2, xtoken2, content):
    aheaders = {'Content-Type': 'application/json; charset=UTF-8',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
                'Cookie': cookie2,
                'x-token': xtoken2,
                'Referer': 'http://jeacher.com'
                }
    blogurl = 'http://jeacher.com/content/insert/idea'
    # id 修改指定文档用的
    adata = {
        'content': content,
        'contentType': '3',
        'status': '2'  # status 1草稿 2发布
    }

    response = requests.post(blogurl, headers=aheaders, json=adata)
    print(response.text)


if __name__ == '__main__':
    cookie2 = 'loginAccount=15732164757; phone=15732164757; token=619f3ccce41c10d2bc1dac9bc43b9bab674fc986e1d315adc56f466b53180dee1e3788726fdc521d0a46535cfa01ca9a78d8e4713ad97e8669f1d10fb9a63bca8861b4a56ca818c1987d363a3f70c3a77c8a6d7250dd6ab0c9d7d6c0f8fe0512e0884117575bc2d338c0b6e464bfeca9afc31b9f21d4b122fbf73e50b0044958c142518a0d0ba0bd9ff09922d57eb09b5ec1bece3b32dce080b0e249074758ebf8391970a04efda8942df028026677fd8f815f2beee8b24fdd6734c1f0ee41a242076a68ed48f67a1a7d5d2d07afd435732ca1af9e548ba4617ba0952c4295f61ad6e3e31d61d4f206e9950f837b1673474d09fdbc8290a68ff4306743d0a3fa8bf854a74e4120b8a5dfdcd6b0a81b0aab04db09e6d9ed751228fa54390d19824b73f2ff7d6e1c597885eea5dc917cba8467102ad57c87b314257f83efaf8d22bd84f513618d93d32c2040689ac1bb8407136e6bfffbd7e816ade447285ca4365f5acb5551ef2e080d553c2e6bd6320b8fc0d19d8548c22e6b4fa2af34e8cd1f'
    xtoken2 = '619f3ccce41c10d2bc1dac9bc43b9bab674fc986e1d315adc56f466b53180dee1e3788726fdc521d0a46535cfa01ca9a78d8e4713ad97e8669f1d10fb9a63bca8861b4a56ca818c1987d363a3f70c3a77c8a6d7250dd6ab0c9d7d6c0f8fe0512e0884117575bc2d338c0b6e464bfeca9afc31b9f21d4b122fbf73e50b0044958c142518a0d0ba0bd9ff09922d57eb09b5ec1bece3b32dce080b0e249074758ebf8391970a04efda8942df028026677fd8f815f2beee8b24fdd6734c1f0ee41a242076a68ed48f67a1a7d5d2d07afd435732ca1af9e548ba4617ba0952c4295f61ad6e3e31d61d4f206e9950f837b1673474d09fdbc8290a68ff4306743d0a3fa8bf854a74e4120b8a5dfdcd6b0a81b0aab04db09e6d9ed751228fa54390d19824b73f2ff7d6e1c597885eea5dc917cba8467102ad57c87b314257f83efaf8d22bd84f513618d93d32c2040689ac1bb8407136e6bfffbd7e816ade447285ca4365f5acb5551ef2e080d553c2e6bd6320b8fc0d19d8548c22e6b4fa2af34e8cd1f'
    page = 12
    for i in range(page, page + 1):
        rest = requests.get("http://m2.qiushibaike.com/article/list/text?type=refresh&page={}&count=12".format(page),
                            headers={
                                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.134 Safari/537.36 Edg/103.0.1264.71",
                            })
        json_data = rest.json()
        # print(json_data)
        for item in json_data['items']:
            content = item['content']
            print(item['user']['login'] + content)
            # imgurl=item['user']['medium']
            # if '.png' in imgurl or '.jpg' in imgurl or '.jpeg' in imgurl:
            #     upload(cookie2, xtoken2, '糗事百科', imgurl)
            # putBlogs(xtoken2, xtoken2, content)

import os
import sys
import urllib
from urllib.request import urlretrieve
import requests


def download():
    category = 'iqiyi'  # 需要下载图片所对应的txt文件
    # 将下载到的图片保存在文件夹dataset中
    os.makedirs('./download/%s' % category, exist_ok=True)
    # txt文件所在路径
    with open('./%s.m3u8' % category, 'r') as file:
        urls = file.readlines()
        n_urls = len(urls)
        for i, url in enumerate(urls):
            if url.startswith("https://"):
                try:
                    splll = url.split('.ts?')[0].split('/videos/')[1]
                    'https://v-7cc09135.71edge.com/videos/vts/20210704/cf/7a/c7e1eff7a45e27280ee89a67a58f0c87.ts?key=0c7872d1672b2840ebd3d47bb36c67d35&dis_k=26c433e6c3639c8e1861d9912b3662445&dis_t=1629095170&dis_dz=GWBN-BeiJing&dis_st=49&src=iqiyi.com&dis_hit=0&dis_tag=01010000&uuid=d367b3f3-611a0502-122&qd_tvid=8213877469141500&contentlength=1310172&end=111845524&qd_p=0&qd_uid=0&qd_vipres=0&qd_vip=0&qd_index=vod&qd_src=03020031010000000000&qd_tm=1629083457352&sd=9834680&start=110535352&qd_k=4f473ca585c52c80b3a46fda29c05cae&z=beijing11_dxt&abs_speed=500' \
                        .replace('vts/20210704/cf/7a/c7e1eff7a45e27280ee89a67a58f0c87', splll)
                    dodownload(url, i, n_urls)
                    print('%s %i/%i' % (category, i, n_urls))
                except:
                    print('%s %i/%i' % (category, i, n_urls), 'no image')


# 做下载
def dodownload(fileurl, postion, tatol):
    # r = requests.get(fileurl)# 你这只是返回string
    # with open(os.path.join(os.path.dirname(os.path.abspath("__file__")), str(postion) + ".ts"), "wb") as f:
    # with open(os.path.join(os.getcwd()+"/download/iqiyi", str(postion) + ".ts"), "wb") as f:
    #     f.write(r.content)
    save_path = os.path.join(os.getcwd() + "/download/iqiyi", str(postion) + ".ts") # 保存路径
    urllib.request.urlretrieve(fileurl, save_path)
    sys.stdout.write('\r>> Downloading %.1f%%' % (float(postion + 1) / float(tatol) * 100.0))
    sys.stdout.flush()
    print("\n完成" + str(postion))


if __name__ == '__main__':
    download()

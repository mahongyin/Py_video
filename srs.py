import cv2
import subprocess

# 目标IP：SRS服务器地址（此处用的是本地运行的SRS）
rtmp = 'rtmp://127.0.0.1:1935/live/livestream'

# 可以推本地摄像头的流
cap = cv2.VideoCapture(0)
# 也可以推RTMP流，这里以获取CCTV为例
# cap = cv2.VideoCapture('rtmp://media3.scctv.net/live/scctv_800')

size = (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))
sizeStr = str(size[0]) + 'x' + str(size[1])

command = ['ffmpeg',
           '-y', '-an',
           '-f', 'rawvideo',
           '-vcodec', 'rawvideo',
           '-pix_fmt', 'bgr24',
           '-s', sizeStr,
           '-r', '25',
           '-i', '-',
           '-c:v', 'libx264',
           '-pix_fmt', 'yuv420p',
           '-preset', 'ultrafast',
           '-f', 'flv',
           rtmp]

pipe = subprocess.Popen(command , shell=True, stdin=subprocess.PIPE)
while cap.isOpened():
    success, frame = cap.read()
    if success:
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        pipe.stdin.write(frame.tostring())

cap.release()
pipe.terminate()

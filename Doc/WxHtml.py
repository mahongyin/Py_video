import wx
import wx.html2 as html


class MyBrowser(wx.Dialog):

    def __init__(self, *args, **kwds):
        wx.Dialog.__init__(self, *args, **kwds)
        sizer = wx.BoxSizer(wx.VERTICAL)
        self.browser = wx.html2.WebView.New(self)
        sizer.Add(self.browser, 1, wx.EXPAND, 10)
        self.SetSizer(sizer)
        self.SetSize((960, 720))


if __name__ == '__main__':
    app = wx.App()
    dialog = MyBrowser(None, -1)
    # dialog.browser.LoadURL("http://www.baidu.com/?tn=62095104_10_oem_dg")  # 加载页面
    dialog.browser.LoadURL("file://D:/Downloads/contentDetail.html")  # 加载页面
    # dialog.browser.LoadURL("file://D:/Downloads/blogsEditor.html")  # 加载页面
    dialog.Show()
    app.MainLoop()

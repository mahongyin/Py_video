import time
import requests
import parsel
import csv

import yaml
if __name__ == '__main__':
    f = open('公众号文章.csv', mode='a', encoding='utf-8', newline='')
    csv_writer = csv.DictWriter(f, fieldnames=['标题', '公众号', '文章发布时间', '文章地址'])
    csv_writer.writeheader()

    with open("wechat.yaml", "r") as file:
        file_data = file.read()
    config = yaml.safe_load(file_data)

    for page in range(1, 2447):
        url = f'https://weixin.sogou.com/weixin?query=python&_sug_type_=&s_from=input&_sug_=n&type=2&page={page}&ie=utf8'
        headers = {
            "Cookie": config['cookie'],
            "User-Agent": config['user_agent'],
            'Host': 'weixin.sogou.com',
            'Referer': 'https://www.sogou.com/web?query=python&_asf=www.sogou.com&_ast=&w=01019900&p=40040100&ie=utf8&from=index-nologin&s_from=index&sut=1396&sst0=1610779538290&lkt=0%2C0%2C0&sugsuv=1590216228113568&sugtime=1610779538290'
        }
        response = requests.get(url=url, headers=headers)
        selector = parsel.Selector(response.text)
        lis = selector.css('.news-list li')
        for li in lis:
            title_list = li.css('.txt-box h3 a::text').getall()
            num = len(title_list)
            if num == 1:
                title_str = 'python' + title_list[0]
            else:
                title_str = 'python'.join(title_list)

            href = li.css('.txt-box h3 a::attr(href)').get()
            article_url = 'https://weixin.sogou.com' + href
            name = li.css('.s-p a::text').get()
            date = li.css('.s-p::attr(t)').get()
            timeArray = time.localtime(int(date))
            otherStyleTime = time.strftime("%Y-%m-%d %H:%M:%S", timeArray)
            dit = {
                '标题': title_str,
                '公众号': name,
                '文章发布时间': otherStyleTime,
                '文章地址': article_url,
            }
            csv_writer.writerow(dit)
            print(title_str, name, otherStyleTime, article_url)

# coding=utf-8
import struct
import traceback
from io import BytesIO
from time import sleep
from zlib import decompress
from threading import Thread
from socket import (socket, AF_INET, SOCK_DGRAM, SOL_SOCKET, SO_BROADCAST)
from tkinter import Tk, BOTH, YES, Label  # 导入tkinter模块

from PIL.Image import frombytes
from PIL import Image, ImageTk  # 导入PIL模块中的Image、ImageTk

# url = r'https://mp.weixin.qq.com/s/'
# startfile(url)

root = Tk()
root.title('屏幕广播接收')
root.geometry('1000x600+150+100')
root.attributes('-topmost', True)
# 使用Label 显示图像，自带双缓冲
lbImage = Label(root)
lbImage.pack(fill=BOTH, expand=YES)
BUFFER_SIZE = 60 * 1024
PORT = 22222


def show_image(image_bytes, image_size):
    global img  # 必须使用全局变量 否则不显示图像
    # 窗口尺寸
    screen_width = root.winfo_width()
    screen_hight = root.winfo_height()
    # 重建图像，如果图像数据不完整直接放弃
    try:
        img = frombytes('RGB', image_size, image_bytes)
        # bytes_stream = BytesIO(image_bytes)
        # img = Image.open(bytes_stream.getvalue())//utf-8 编码出错
        # print(list(img.getdata()))
        # img.save(r'save2.png')
    except Exception as e:
        traceback.print_exc()
        print(e)
        path = "testpic.png"  # 图片路径
        img = Image.open(path)
        # return
    # 显示图像
    img = img.resize((screen_width, screen_hight))
    photo = ImageTk.PhotoImage(img)  # 创建tkinter兼容的图片
    lbImage.config(image=photo)  # 通过Image=photo设置要展示的图片
    lbImage.image = photo


def recv_image():
    global receiving, thread_show, image_data, image_size, data_size
    # 创建UDP套接字，绑定用来接受屏幕广播
    sock = socket(AF_INET, SOCK_DGRAM)
    sock.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
    sock.bind(('', PORT))
    # sock.bind(('0.0.0.0', PORT))
    while receiving:
        # 开始接收
        fheaf_size = struct.calcsize('lii')  # 数据长度 long int int
        rdata, raddr = sock.recvfrom(fheaf_size)
        w = 720
        h = 480
        if rdata:
            datas=struct.unpack('lii', rdata)
            data_size = datas[0]
            w = datas[1]
            h = datas[2]
            print(data_size,w,h)
        recvd_size = 0
        data_total = b''
        while not recvd_size == data_size:
            if data_size - recvd_size > BUFFER_SIZE:
                cdata, caddr = sock.recvfrom(BUFFER_SIZE)
                recvd_size += len(cdata)
            else:
                cdata, caddr = sock.recvfrom(BUFFER_SIZE)
                recvd_size = data_size
            data_total += cdata
        # 图像接受
        image_data = decompress(data_total)
        # print("{} kb\n{} Mb".format(size / 1e3, size / 1e6))
        image_size = [w, h]
        # 显示图片
        # show_image(image_data, image_size)
        thread_show = Thread(target=show_image, args=(image_data, image_size))
        thread_show.daemon = True
        thread_show.start()


receiving = True
thread_sender = Thread(target=recv_image)
thread_sender.daemon = True
thread_sender.start()


# 退出程序时 先停止接受在推出
def close_window():
    global receiving
    receiving = False
    sleep(0.3)
    root.destroy()


if __name__ == '__main__':
    root.protocol('WM_DELETE_WINDOW', close_window)
    root.mainloop()

import socket
import socketserver
import tkinter
from threading import Thread
from tkinter import Tk, BooleanVar, Button, Label, scrolledtext

global udpCliSock
global BUFSIZ


def get_host_ip():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(('8.8.8.8', 80))
        ip = s.getsockname()[0]
    finally:
        s.close()
    return ip


def getip():
    # 获取计算机名称
    hostname = socket.gethostname()
    # 获取本机IP
    ip = socket.gethostbyname(hostname)
    return ip


class MyUDPHandler(socketserver.BaseRequestHandler):

    def handle(self):
        print("{}\t{}".format(self.client_address[0], str(self.request[0].strip(), 'utf-8')))
        scro_txt.insert(tkinter.END, '123')  # 从末尾添加


if __name__ == "__main__":
    root = Tk()
    root.title('Log接收模块')
    # 初始尺寸和位置
    root.geometry('960x640+300+120')
    # 两个方向都不允许缩放
    root.resizable(False, False)
    tkinter.Label(root, text=get_host_ip()).grid(row=1, column=2)
    tkinter.Label(root, text="ServerIP").grid(row=1, column=1)
    tkinter.Label(root, text="目标Port").grid(row=2, column=1)
    port = tkinter.IntVar()
    port.set(9696)
    BUFSIZ = 1024
    tkinter.Entry(root, textvariable=port).grid(row=2, column=2)

    udpCliSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    scro_txt = scrolledtext.ScrolledText(root, width=100, height=40)
    scro_txt.grid(row=4, column=3)


    # scro_txt.insert(1.0, 'abc')  #插入第一条
    # scro_txt.insert(tkinter.END, '123')  #从末尾添加

    def stop():
        udpCliSock.close()
        btnStart['text'] = "开始接收"

    def btnStartClick():
        # HOST, PORT = "0.0.0.0", port.get()
        # server = socketserver.UDPServer((HOST, PORT), MyUDPHandler)
        # server.serve_forever()
        # print('server running')
        HOST = '0.0.0.0'
        ADDR = (HOST, port.get())
        udpCliSock.bind(ADDR)
        btnStart['text'] = "已连接"
        while True:
            print('waiting for message...')
            data, addr = udpCliSock.recvfrom(BUFSIZ)  ##监听并接受客户端发的消息
            if not data:
                break
            print(data.decode())
            scro_txt.insert(tkinter.END, data.decode() + "\n")  # 从末尾添加
            # print('...received from and returned to:', addr)


    def thread1():
        btnStart['state'] = 'disabled'
        t2 = Thread(target=btnStartClick, name='LoopThread')
        t2.start()


    btnStart = Button(root, text="开始接收", command=thread1, bg='white', activeforeground='red')
    btnStart.grid(row=3, column=1)

    root.mainloop()

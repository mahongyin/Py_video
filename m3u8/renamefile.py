# coding=gbk
import os
import sys


def rename():
    path = input("请输入路径(例如D:\\\\picture)：")
    name = input("请输入开头名:")
    startNumber = input("请输入开始数:")
    fileType = input("请输入后缀名（如 .jpg、.txt等等）:")
    print("正在生成以" + name + startNumber + fileType + "迭代的文件名")
    count = 0
    filelist = os.listdir(path)
    for files in filelist:
        Olddir = os.path.join(path, files)
        if os.path.isdir(Olddir):
            continue
        Newdir = os.path.join(path, name + str(count + int(startNumber)) + fileType)
        os.rename(Olddir, Newdir)
        count += 1
    print("一共修改了" + str(count) + "个文件")


# 00001.jpg
# 00002.jpg
# 00003.jpg
# 为了示例方便，将待修改与当前工作目录取同一个
def ReName():
    fileList = os.listdir(r"D:\PyProjects\video\m3u8\download\iqiyi")  # 待修改文件夹
    print("修改前：" + str(fileList))  # 修改前的文件
    currentPath = os.getcwd()  # 得到进程当前工作目录
    os.chdir(r"D:\PyProjects\video\m3u8\download\iqiyi")  # 将当前工作目录修改为待修改文件夹的位置
    for i in range(0, len(fileList)):
        os.rename(fileList[i], str(("%03d" % i)) + '.' + 'ts')  # 文件重新命名
        # 如果想实现000000~999999，只需将这里的5改为6，诸如此类。
    # print("\n")
    os.chdir(currentPath)  # 改回程序运行前的工作目录
    sys.stdin.flush()  # 更新


# print("修改后："+str(os.listdir(r"D:\PyProjects\video\m3u8\download\iqiyi")))		#修改后的文件

# xxx000000.jpg
# xxx000001.jpg
# xxx000002.jpg
def rename2():
    path = "需要重命名的文件所在文件夹"
    filelist = os.listdir(path)
    filelist.sort()
    for file in filelist:
        Olddir = os.path.join(path, file)
        if os.path.isdir(Olddir):
            continue
    filename = os.path.splitext(file)[0]  # 文件名 例如img0
    filename = filename[3:]  # 忽略文件名前3位 取后面数字字符串
    filetype = os.path.splitext(file)[1]  # 文件后缀名 例如.jpg
    Newdir = os.path.join(path, "xxx" + filename.zfill(6) + filetype)  # 6位整数
    os.rename(Olddir, Newdir)


def renamets():
    fileList = os.listdir(r"D:\PyProjects\video\m3u8\download\iqiyi")  # 待修改文件夹
    currentPath = os.getcwd()  # 得到进程当前工作目录
    os.chdir(r"D:\PyProjects\video\m3u8\download\iqiyi")  # 将当前工作目录修改为待修改文件夹的位置
    for i in range(0, len(fileList)):
        tslen = len(fileList[i].split('.ts')[0])
        if tslen == 1:
            os.rename(fileList[i], '000' + fileList[i])  # 文件重新命名
        elif tslen == 2:
            os.rename(fileList[i], '00' + fileList[i])  # 文件重新命名
        elif tslen == 3:
            os.rename(fileList[i], '0' + fileList[i])  # 文件重新命名
    # print("\n")
    os.chdir(currentPath)  # 改回程序运行前的工作目录
    sys.stdin.flush()  # 更新


if __name__ == '__main__':
    # rename()
    # rename2()
    ReName()
    # renamets()

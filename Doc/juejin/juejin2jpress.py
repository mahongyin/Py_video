# coding:utf-8
import json
import os
import re
import time

import regex
import requests
from bs4 import BeautifulSoup
from requests_toolbelt import MultipartEncoder

mycookie = 'csrf=FWBspGlf5wToZvI; Hm_lvt_bfe2407e37bbaa8dc195c5db42daf96a=1658473305; Hm_lpvt_c222a05e965a074b1e497d3178c74667=1658565871; Hm_lvt_c222a05e965a074b1e497d3178c74667=1658565871; _jpanonym="NTI5ZmViNDYzOWM3Y2I4NWNhMTcyZGE4MDVjOWMwOGYjMTY1ODgyNzI1ODg4OCMzMTUzNjAwMCNZek5qTnpWaU56RTJNMkkwTkdGbU16bG1NemM0TldReU1HSTFZV1psTURVPQ=="; atlassian.xsrf.token=B5YS-P1PN-6TX7-DYR8_296cba8df699715f175981c5657a217822783165_lout; JSESSIONID=58D10237BAFACAB4BF50FDB51A1DD3BA; PHPSESSID=55dab73304b9d841606869b1; csrf_token=819582cf33aa5e6ec95668a7239c8a7a; Hm_lpvt_bfe2407e37bbaa8dc195c5db42daf96a=1660956989; _jpuid="NmE3MDM1ZDllMmE0MGE0ODA4MzVhNDAxNDc4YTgzOTAjMTY2MDk1ODE4NjMzOCMxNzI4MDAjTVE9PQ=="'
csrf_token = '819582cf33aa5e6ec95668a7239c8a7a'


def headers():
    return {
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
        "Cookie": mycookie,
        "Host": "tencent.a1000.top:8080",
        "Origin": "http://tencent.a1000.top:8080",
        "Referer": "http://tencent.a1000.top:8080/admin/article/write",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.60 Safari/537.36",
        "X-Requested-With": "XMLHttpRequest"
    }


def bodys(title2, summary, content):
    return {
        "article.status": "normal",
        "article.id": "",
        "article.user_id": "1",
        "article.edit_mode": "html",
        "article.title": title2,
        "article.slug": "",
        "article.content": content,
        "article.summary": summary,
        "article.meta_keywords": "",
        "article.meta_description": "",
        "article.order_number": "",
        "article.link_to": "",
        "article.created": "",
        "article.comment_status": "false",
        "category": "6",
        "article.thumbnail": "/attachment/20220723/7c84d24e16704268b252b113a86a332f.jpg",
        "article.style": "",
        "article.flag": "",
        "csrf_token": csrf_token
    }


def download_img(title, img_url):
    if not img_url.startswith('http'):
        img_url = 'https:'+img_url
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36'
    }
    r = requests.get(img_url, headers=headers, stream=True)
    # print(r.status_code) # 返回状态码
    fileName = ''
    if r.status_code == 200:
        if not os.path.isdir("./img/" + title):
            os.makedirs("./img/" + title)
        # 截取图片文件名
        img_name = img_url.split('#')[0].split('?')[0].split('/').pop()
        if '.png' in img_name or '.jpg' in img_name or '.jpeg' in img_name or '.gif' in img_name or '.webp' in img_name or '.image' in img_name:
            fileName = "./img/" + title + "/" + img_name
            with open(fileName, 'wb') as f:
                f.write(r.content)
            print("download img", fileName)
            # 去水印
            # clearWater2(fileName)
        return fileName
    else:
        return fileName


# 去除文件名中的特殊字符 +-*#&!~^%$
def fixname(filename):
    intab = r'[?*/\|.:><]'
    filename = re.sub(intab, "", filename)  # 用正则表达式去除windows下的特殊字符，这些字符不能用在文件名
    return filename


def upload(title, img):
    title = re.sub('[^\w\-_\. ]', ' ', title)
    # print(title)
    name = download_img(title, img)
    if len(name) > 0:
        # t = time.time()
        # timestamp = int(round(t * 1000))  # 13位
        res = uploadimg(name)
        # 'application/octet-stream'
        if res.status_code == 200:
            print('上传成功' if res.text == '' else eval("u" + "\'" + res.text + "\'"))
            return json.loads(res.text)['src']
        # {"src":"/attachment/20220807/d3c51a6a84f4449f9543934fea898375.png","success":true,"state":"ok","attachmentId":134,"title":"Snipaste_2022-07-20_19-52-33.png"}
        # print(eval("u" + "\'" + res.text + "\'"))
        print('上传失败' if res.text == '' else eval("u" + "\'" + res.text + "\'"))
        return ""
    else:
        return ""


# status 1草稿 2发布
def putBlogs(title, beirf, html):
    blogurl = "http://tencent.a1000.top:8080/admin/article/doWriteSave?csrf_token="+csrf_token
    # print(bodys(title, beirf, html))
    response = requests.post(blogurl, headers=headers(), data=bodys(title, beirf, html))
    print('发布完成：'+response.text)


# <img src="https://www.runoob.com/wp-content/uploads/2013/12/20171102-1.png">
# <img class="imgBox" src="http://jeacher-assets.oss-cn-beijing.aliyuncs.com/blogs/165867134571340.png">
def getContentDiv(bgurl):
    headers = {
        "referer": "https://juejin.cn/",
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.134 Safari/537.36 Edg/103.0.1264.71"
    }
    res = requests.get(bgurl, headers=headers)
    cdata = res.content #.decode('utf-8').strip()
    # print(cdata)
    cont = BeautifulSoup(cdata, 'html.parser')
    # 获取包含文章内容的标签 attrs后跟的是最外层标签属性，根据爬取网站的实际情况进行修改 id:content  class:mainxx
    # data = cont.find('div', attrs={'class': 'article-body'}).text
    data = cont.find('div', attrs={'id': 'juejin'})  # 掘金article-content
    headt = cont.find("title").text
    # headt = datad.find("h1", attrs={'class': 'article-title'}).text
    # data = datad.find('div', attrs={'class': 'markdown-body'})  # 掘金
    print(data)
    if len(headt) > 15:
        # if len(myh1) > 20:
        #     title = myh1[:19]
        # else:
        title = headt.strip()
    else:
        struct_time = time.localtime(time.time())  # 得到结构化时间格式str(time.time())
        now_time = time.strftime("%Y-%m-%d %H-%M-%S", struct_time)
        title = headt.strip() + "-" + now_time

        # 图操作
    for imgv in data.find_all('img'):
        yimg = imgv["src"]
        if '.png' in yimg or '.jpg' in yimg or '.jpeg' in yimg or '.gif' in yimg or '.webp' in yimg or '.awebp' in yimg or '.image' in yimg:
            print(yimg)
            ossimg = upload(title, yimg)
            if len(ossimg) > 0:
                imgv["src"] = ossimg
    print('*************************************************\n')
    data.prettify()

    # print(cont.find('div', class_="article-body"))
    value = cont.find('div', attrs={'id': 'juejin'})

    # exp = exportContent.Extractor(bgurl, blockSize=5, image=False)
    # beirf = exp.getContext()[:150]
    beirf = value.text.strip()[:150]
    # print(value) #去除首尾空格
    valueStr = str(value).strip()  # .replace("\\n", "").replace("\\r", "").replace("\\ss", "")
    # print(title, valueStr)
    putBlogs(title, beirf, valueStr)


# suffix  .png
def uploadimg(img_path):
    url1 = 'http://tencent.a1000.top/commons/attachment/upload?csrf_token='+csrf_token
    # from_data上传文件，注意参数名uploadFile
    filename, suffix = os.path.splitext(img_path)
    fname = str(filename).split("/").pop() + suffix
    opf = open(img_path, 'rb')
    data = MultipartEncoder(
        fields={'upload': (fname, opf, 'image/*')})
    aheaders = {
        'Content-Type': data.content_type,
        'Cookie': mycookie,
        'Host': "tencent.a1000.top:8080",
        'Origin': "http://tencent.a1000.top:8080",
        'Referer': "http://tencent.a1000.top:8080/admin/article/write",
        'User-Agent': "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.60 Safari/537.36"
    }
    res0 = requests.post(url=url1, data=data, headers=aheaders)
    opf.close()
    return res0

    # # raw上传文件
    # file = open('D:/Pictures/疾书/22222.png', 'rb')
    # res1 = requests.post(url=url1, data=file.read(), headers={'Content-Type': 'image/png'})
    # print(res1.text)
    #
    # # binary上传文件
    # files = {'file': open('D:/Pictures/疾书/22222.png', 'rb')}
    # res2 = requests.post(url=url1, files=files, headers={'Content-Type': 'binary'})
    # print(res2.text)

    # fl = open(r'd:/out/50229.jpg', 'rb')
    # rs = requests.post(url1, files={'file': ('name50229.jpg', fl, 'application/octet-stream'), 'name': (None, '1.jpg\n')})


def getDiv(mdpath):
    file_object2 = open(mdpath, 'r', encoding="UTF-8")
    index = 0
    arr = []
    # rex = """(?|(?<txt>(?<url>(?:ht|f)tps?://\S+(?<=\P{P})))|\(([^)]+)\)\[(\g<url>)\])"""
    rex = "(?<=\\]\\()[^\\)]+"  # 匹配]()中间
    # rex = "(?<=\\]().*?(?=\\))"  # 匹配]()区间但不包含()
    try:
        lines = file_object2.readlines()
        print("type(lines)=", type(lines))  # type(lines)= <type 'list'>
        for line in lines:
            # print("line=", line)
            pattern = regex.compile(rex)
            matches = regex.findall(pattern, line, overlapped=True)
            for herf in matches:
                index += 1
                arr.append(herf)
                # arr.insert(index,herf)
                print(index, herf)
    finally:
        file_object2.close()
    return arr

if __name__ == "__main__":
    # upload("测试", "https://img2020.cnblogs.com/blog/1210268/202110/1210268-20211004164242856-179706561.png")
    getContentDiv('https://juejin.cn/post/7125740435506462734')

def addVideo():
    v1='''
    <video id="welcome" height="100%" width="100%" preload="auto" loop="" autoplay="" poster="http://lorempixel.com/150/100/abstract/1/">
   <source type="video/mp4" src="http://www.808.dk/pics/video/gizmo.mp4">
   <img src="http://lorempixel.com/150/100/abstract/1/" height="150" width="100" alt="" title="Your browser does not support the <video> tag">
</video>'''
    v2='''<iframe src="//player.bilibili.com/player.html?aid=215680244&amp;bvid=BV1ea411p7rt&amp;cid=765711961&amp;page=1" scrolling="no" border="0" frameborder="no" framespacing="0" allowfullscreen="true"> </iframe>'''


if __name__ == "__main__1":
    urls = getDiv("md/stars-one-blog-list.md")
    # urls = ['https://www.cnblogs.com/stars-one/p/15270435.html']
    for url in urls:
        # print(url)
        getContentDiv(url)
        time.sleep(1)

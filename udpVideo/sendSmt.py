# -*- coding: utf-8 -*-
import sys
import socket


#  注释的标注格式为：每行开头则直接输入#并空一格开始注释
#  若在每行代码后面加注释，则空两格输入#再空一格开始注释

#  使用udp收发数据
def main():
    #  1.创建socket套接字
    udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)  # AF_INET表示使用ipv4,默认不变，SOCK_DGRAM表示使用UDP通信协议

    #  2.绑定端口port
    local_addr = ("", 7878)  # 默认本机任何ip ，指定端口号7878
    udp_socket.bind(local_addr)  # 绑定端口

    #  3.收发数据
    send_data = input("请输入您想要发送的数据：")
    udp_socket.sendto(send_data.encode("utf-8"), ("192.168.32.111", 8090))  # 编码成全球统一数据格式，用元组表示接收方ip和port
    recv_data = udp_socket.recvfrom(1024)  # 定义单次最大接收字节数

    #  4.打印数据
    recv_msg = recv_data[0]  # 接收的元组形式的数据有两个元素，第一个为发送信息
    send_addr = recv_data[1]  # 元组第二个元素为发信息方的ip以及port
    print("收到的信息为：", recv_msg.decode("gbk"))  # 默认从windows发出的数据解码要用”gbk”，保证中文不乱码
    print("发送方地址为：", str(send_addr))  # 强转为字符串输出地址，保证不乱码

    #  5.关闭套接字
    udp_socket.close()


# python sendSmt.py  参数1 参数2    sys.argv[0]是脚本名
if __name__ == '__main__':
    print('Programe name:{}'.format(sys.argv[0]))
    for i in range(1, len(sys.argv)):
        print('arg{} {}'.format(i, sys.argv[i]))

    main()

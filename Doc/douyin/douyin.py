from urllib import request
import re
import requests


class DyDownload():
    def desc(self, desc_data):
        # 将文案内容保存到txt文档内
        # #根据文案长度提取特定长度的文字作为文件名
        if len(desc) > 20:
            desc_name = desc[1:20]
        else:
            desc_name = desc[1:10]
        f = open(desc_name + '.txt', 'w', encoding='utf-8')
        f.write(desc_data)
        f.close()
        print("文案保存成功")

    def music(self, name, url):
        # 保存音乐
        r = requests.get(url)
        with open(name + '.mp3', 'wb') as f:
            f.write(r.content)
        print("音乐保存成功")

    def images(self, name, url):
        # 获取图片并保存图片
        r = requests.get(url)
        with open(name + '.jpg', 'wb') as f:
            f.write(r.content)


Download = DyDownload()
api = "https://www.douyin.com/web/api/v2/aweme/iteminfo/?item_ids="

# 图文分享链接
short_url = input("请输入抖音图文短链接")

# 短连接解析并且取出内容ID
content_id = re.findall(r"\b\d+\b", request.urlopen(short_url).geturl())[0]

# 设置请求头
headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36"}
# 请求返回的json数据
response = requests.get(api + content_id, headers=headers).json()

# 获取图片数量
image_quantity = response["item_list"][0]["images"]

if image_quantity is None:
    print(f"{content_id}不是图文无法下载")
else:
    print("正在获取图文信息..")
    # 获取图片列表
    images_content = response["item_list"][0]["images"]
    # 获取账号主昵称
    nickname = response["item_list"][0]["author"]["nickname"]
    # 获取音乐名称
    music_name = response["item_list"][0]["music"]["title"]
    # 获取音乐下载地址
    music_url = response["item_list"][0]["music"]["play_url"]["uri"]
    # 获取文案
    desc = response["item_list"][0]["desc"]

    Download.desc(desc)
    Download.music(music_name, music_url)

    for i in range(len(images_content)):
        images_url = images_content[i]["url_list"][3]
        Download.images(str(i), images_url)
        print(f"{str(i)}号图片保存成功")
    print("文案，音乐，图片，任务全部完成")
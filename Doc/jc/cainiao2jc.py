# coding:utf-8
import os
import re
from bs4 import BeautifulSoup
import requests
import json
import time

import cv2
import numpy as np

from itertools import product
from PIL import Image
from requests_toolbelt import MultipartEncoder


# pip install requests-toolbelt

# img_url = "http://www.py3study.com/Public/images/article/thumb/random/48.jpg"
# ret = download_img(img_url)
# if not ret:
#     print("下载失败")
# print("下载成功")

def download_img(img_url):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36'
    }
    r = requests.get(img_url, headers=headers, stream=True)
    # print(r.status_code) # 返回状态码
    fileName = ''
    if r.status_code == 200:
        if not os.path.isdir("./img"):
            os.makedirs("./img")
        # 截取图片文件名
        img_name = img_url.split('/').pop()
        fileName = "./img/" + img_name
        with open(fileName, 'wb') as f:
            f.write(r.content)
        print("download img", fileName)
        # 去水印
        clearWater(fileName)
        return fileName
    else:
        return fileName


# 去除水印
def clearWater(imgPath):
    img = cv2.imread(imgPath)
    new = np.clip(1.4057577998008846 * img - 38.33089999653017, 0, 255).astype(np.uint8)
    cv2.imwrite(imgPath, new)


def clearWater2(imgPath):
    img = Image.open(imgPath)
    width, height = img.size
    for pos in product(range(width), range(height)):
        if sum(img.getpixel(pos)[:3]) > 600:
            img.putpixel(pos, (255, 255, 255))
    img.save(imgPath)


# def uposs(yourLocalFile):
#     # 阿里云主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建RAM账号。
#     auth = oss2.Auth('<yourAccessKeyId>', '<yourAccessKeySecret>')
#     # Endpoint以beijing为例，其它Region请按实际情况填写。
#     bucket = oss2.Bucket(auth, 'http://oss-cn-beijing.aliyuncs.com', '<yourBucketName>')  # jeacher
#
#     # 必须以二进制的方式打开文件，因为需要知道文件包含的字节数。
#     with open(yourLocalFile, 'rb') as fileobj:
#         # Seek方法用于指定从第1000个字节位置开始读写。上传时会从您指定的第1000个字节位置开始上传，直到文件结束。
#         fileobj.seek(1000, os.SEEK_SET)
#         # Tell方法用于返回当前位置。
#         current = fileobj.tell()
#         bucket.put_object('<yourObjectName>', fileobj)


def upload(cookie, xtoken, img):
    name = download_img(img)
    if len(name) > 0:
        res = uploadimg(cookie, xtoken, name)
        if res.status_code == 200:
            print('上传成功' if res.text == '' else eval("u" + "\'" + res.text + "\'"))
            # return json.loads(res.text)['img_url']
            return json.loads(res.text)['data']
        # print(eval("u" + "\'" + res.text + "\'"))
        return ""
    else:
        return ""


# status 1草稿 2发布
def putBlogs(title, content, html):
    cookie0 = 'phone=10000000000; token=930c7c291ade1753df2c7ba125e339ba4f3fc51fe2a0527bfc20b4976aac770085239b4936cfa8c11aec78479e6d7b485fbe6e5dd9dd4c2c661cf58ffb909ebf8cffc07dbd275548c776b80139152fc596110b5c502e2aab15fbc2e9160e95d0fa0ebc64ec28ca41bc5be57954d31f3f46e272d93bfe9bfe590b2ee888b18799e6a80ff21a2b9c4a932b484190edb62b698d505a39e275ba3aeae55b43a6309200fb2d3947c147c975e99c1cf47554ead69da67182caa521adfa304537d7f258c46a748d8c83647bd7d89f3cd791b69a3614a1d694036da7a7f7b54dcd66c37865f64a18648f4d9f411629158d94cdc87ded01c0ef154529e94b197e881f47d4a9ecf8fa20428cbeb6cc67204678c7cd8fc0d19d8548c22e6b4fa2af34e8cd1f; test=aaaa'
    xtoken0 = '930c7c291ade1753df2c7ba125e339ba4f3fc51fe2a0527bfc20b4976aac770085239b4936cfa8c11aec78479e6d7b485fbe6e5dd9dd4c2c661cf58ffb909ebf8cffc07dbd275548c776b80139152fc596110b5c502e2aab15fbc2e9160e95d0fa0ebc64ec28ca41bc5be57954d31f3f46e272d93bfe9bfe590b2ee888b18799e6a80ff21a2b9c4a932b484190edb62b698d505a39e275ba3aeae55b43a6309200fb2d3947c147c975e99c1cf47554ead69da67182caa521adfa304537d7f258c46a748d8c83647bd7d89f3cd791b69a3614a1d694036da7a7f7b54dcd66c37865f64a18648f4d9f411629158d94cdc87ded01c0ef154529e94b197e881f47d4a9ecf8fa20428cbeb6cc67204678c7cd8fc0d19d8548c22e6b4fa2af34e8cd1f'
    cookie1 = 'test=aaaa; token=619f3ccce41c10d2bc1dac9bc43b9bab674fc986e1d315adc56f466b53180dee26ace56889787eff59dc5fcdf2376a4513edb47bd60f335d76186f8a1b52d44ef0d79405d61cb43add9a71b44cba5256e2afc3e62bc25c18ffcb50c4aa82527ae77a9c90a2ca3268542ff3e9ac11f3c97180bb0ef82e6058cd1964dee217a3375bc6657338d126050a0489ec6ea307abaa715f472ad12587533ae70e37a8036b98d3fa4fdf53c1225e2071c8d02459032f198c2476b929f82ac514194e090c3658afafc2762918a10ba4a45f9cb1601884e08e29ff2cd33a273f4a550cbf2b8ba6ca72efc79ab973ed4b4f02334392eb1be8bc66e7167591b79c678d849f1313cd3af6d7aff4da65976fbe4fab790b7e02b921427886a19b2137f91cc3119e1c666cd0c0f94feb3a2c35a7d68f41eb5d; phone=10000000001'
    xtoken1 = '619f3ccce41c10d2bc1dac9bc43b9bab674fc986e1d315adc56f466b53180dee26ace56889787eff59dc5fcdf2376a4513edb47bd60f335d76186f8a1b52d44ef0d79405d61cb43add9a71b44cba5256e2afc3e62bc25c18ffcb50c4aa82527ae77a9c90a2ca3268542ff3e9ac11f3c97180bb0ef82e6058cd1964dee217a3375bc6657338d126050a0489ec6ea307abaa715f472ad12587533ae70e37a8036b98d3fa4fdf53c1225e2071c8d02459032f198c2476b929f82ac514194e090c3658afafc2762918a10ba4a45f9cb1601884e08e29ff2cd33a273f4a550cbf2b8ba6ca72efc79ab973ed4b4f02334392eb1be8bc66e7167591b79c678d849f1313cd3af6d7aff4da65976fbe4fab790b7e02b921427886a19b2137f91cc3119e1c666cd0c0f94feb3a2c35a7d68f41eb5d'
    cookie2 = 'test=aaaa; token=619f3ccce41c10d2bc1dac9bc43b9bab674fc986e1d315adc56f466b53180dee26ace56889787eff59dc5fcdf2376a4513edb47bd60f335d76186f8a1b52d44ef0d79405d61cb43add9a71b44cba5256e2afc3e62bc25c18ffcb50c4aa82527abb9ea5c1d81350fadf3777973e8850bbd88d526df033a05f366be4903c53eb715bc6657338d126050a0489ec6ea307abab609e316e9d2b740e98af900f8d728398d3fa4fdf53c1225e2071c8d0245903c6d0baba0da10d15233b3dec24ff6baf27dace63bb355b2e3815676a735a3baf49d6379e7b91f88038e1831fd7f1e47ed5d46a433b82ae2d7d9c3389730704c6837ad6150108096c30a6cf0eb359413a7da74d1b3c88f2732619f429a27ce698340f114419d0b89b3a73324d8409cb4a3769f287f3ae00939f96fc78396b9e012a26eb4b48ea69ae5e192275d2123a11071d9443e559c21745a8ff8f5c6cd969fbc497b5a3dcb7130647c61e487d94c9; phone=10000000002'
    xtoken2 = '619f3ccce41c10d2bc1dac9bc43b9bab674fc986e1d315adc56f466b53180dee26ace56889787eff59dc5fcdf2376a4513edb47bd60f335d76186f8a1b52d44ef0d79405d61cb43add9a71b44cba5256e2afc3e62bc25c18ffcb50c4aa82527abb9ea5c1d81350fadf3777973e8850bbd88d526df033a05f366be4903c53eb715bc6657338d126050a0489ec6ea307abab609e316e9d2b740e98af900f8d728398d3fa4fdf53c1225e2071c8d0245903c6d0baba0da10d15233b3dec24ff6baf27dace63bb355b2e3815676a735a3baf49d6379e7b91f88038e1831fd7f1e47ed5d46a433b82ae2d7d9c3389730704c6837ad6150108096c30a6cf0eb359413a7da74d1b3c88f2732619f429a27ce698340f114419d0b89b3a73324d8409cb4a3769f287f3ae00939f96fc78396b9e012a26eb4b48ea69ae5e192275d2123a11071d9443e559c21745a8ff8f5c6cd969fbc497b5a3dcb7130647c61e487d94c9'
    aheaders = {'Content-Type': 'application/json; charset=UTF-8',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
                'Cookie': cookie2,
                'x-token': xtoken2,
                'Referer': 'http://jeacher.com/jeacher/blogsEditor/blogsEditor.html'}
    # blogurl = "http://47.93.17.8/content/insert/blogs"
    # blogurl = "http://47.93.17.8/content/insert/content"
    blogurl = "http://jeacher.com/content/draft/blogs"  # blogurl = "http://jeacher.com/content/release/blogs"
    adata = {
        'title': title,
        # 'content': content,
        'blogsHtml': html,
        # 'contentType': '2',
        # 'posterUrl': '',
        # 'status': '1',
        'id': '',
    }
    # print(adata)
    # print(title, html.strip())
    # j_str = json.dumps(adata, ensure_ascii=False)
    # j_str2 = re.sub(r'\\n', '', j_str)
    # j_str3 = re.sub(r'\\r', '', j_str2)
    # j_str4 = re.sub(r'\\', '', j_str3)
    # print(j_str4)

    # rdata = eval(json.dumps(j_str4))
    # print(rdata)
    # response = requests.post(blogurl, headers=aheaders, json=rdata)

    response = requests.post(blogurl, headers=aheaders, json=adata)
    print(response.text)


# <img src="https://www.runoob.com/wp-content/uploads/2013/12/20171102-1.png">
# <img class="imgBox" src="http://jeacher-assets.oss-cn-beijing.aliyuncs.com/blogs/165867134571340.png">
def getContentDiv(bgurl):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36'
    }
    res = requests.get(bgurl, headers=headers)
    cdata = res.content
    cont = BeautifulSoup(cdata, 'html.parser')
    # 获取包含文章内容的标签 attrs后跟的是最外层标签属性，根据爬取网站的实际情况进行修改 id:content  class:mainxx
    # data = cont.find('div', attrs={'class': 'article-body'}).text
    data = cont.find('div', attrs={'id': 'content'})  # 菜鸟
    # print(data)
    # print(tables)
    # for table in cont.select('table.reference'):  # class="reference"

    for table in data.find_all("table"):
        table["class"] = "tableBox"
    for tr in data.find_all("tr"):
        tr["class"] = "trBox"
    tds = data.find_all("td")
    for td in tds:
        td["class"] = "tdBox"
    ths = data.find_all("th")
    for th in ths:
        th["class"] = "tdBox"
    for precode in data.find_all('pre'):
        precode["class"] = "codeBox"
    replacement = """
    <pre class="codeBox">
    {text}
    </pre>
    """
    for code in data.select('div.example_code'):
        # print(code.text)
        code.replace_with(BeautifulSoup(replacement.format(text=code.text), 'html.parser'))
    # 图操作
    for imgv in data.find_all('img'):
        yimg = imgv["src"]
        if 'https://www.runoob.com/wp-content/uploads' in yimg:
            print(yimg)
            # ossimg = upload(yimg)
            # if len(ossimg) > 0:
            #     imgv["src"] = ossimg
            #     imgv["class"] = "imgBox"
    print('*************************************************\n')
    data.prettify()

    # print(cont.find('div', class_="article-body"))
    value = cont.find('div', attrs={'id': 'content'})
    myh1 = data.find("h1").text
    if len(myh1) > 13:
        if len(myh1) > 20:
            title = myh1[:19]
        else:
            title = myh1
    else:
        # ['Java基础教程', 'Java面向对象', 'Java高级教程', 'MySql教程']
        title = "版本控制-Git-" + myh1
    # exp = exportContent.Extractor(bgurl, blockSize=5, image=False)
    # beirf = exp.getContext()[:150]
    beirf = value.text.strip()[:150]
    # print(value) #去除首尾空格
    valueStr = str(value).strip()  # .replace("\\n", "").replace("\\r", "").replace("\\ss", "")
    # print(valueStr)
    putBlogs(title, beirf, valueStr)


def getDiv(bgurl):
    # 爬虫
    res = requests.get(bgurl)
    data = res.content
    cont = BeautifulSoup(data, 'html.parser')
    data = cont.find('div', attrs={'id': 'leftcolumn'})  # 菜鸟
    # print(data)
    # print(tables)
    # for table in cont.select('table.reference'):  # class="reference"
    index = 0
    arr = []
    for table in data.find_all("a"):
        index += 1
        herf = 'https://www.runoob.com' + table["href"]
        arr.append(herf)
        # arr.insert(index,herf)
        print(index, herf)
    return arr


# suffix  .png
def uploadimg(cookie, xtoken, img_path):
    url1 = 'http://jeacher.com/content/blogs/image/upload'
    # from_data上传文件，注意参数名uploadFile
    filename, suffix = os.path.splitext(img_path)
    fname = str(filename).split("/").pop() + suffix
    data = MultipartEncoder(
        fields={'suffix': suffix, 'uploadFile': (fname, open(img_path, 'rb'), 'image/*')})
    aheaders = {'Content-Type': data.content_type,
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
                'Cookie': cookie,
                'x-token': xtoken,
                'Referer': 'http://jeacher.com/jeacher/blogsEditor/blogsEditor.html'}
    res0 = requests.post(url=url1, data=data, headers=aheaders)
    return res0

    # # raw上传文件
    # file = open('D:/Pictures/疾书/22222.png', 'rb')
    # res1 = requests.post(url=url1, data=file.read(), headers={'Content-Type': 'image/png'})
    # print(res1.text)
    #
    # # binary上传文件
    # files = {'file': open('D:/Pictures/疾书/22222.png', 'rb')}
    # res2 = requests.post(url=url1, files=files, headers={'Content-Type': 'binary'})
    # print(res2.text)


if __name__ == "__main__":
    # 此处写你要爬虫的url
    urls = [
    ]
    for url in urls:
        getContentDiv(url)
        print('完成 ' + url)
        time.sleep(1)

if __name__ == "__main__1":
    urls = getDiv("https://www.runoob.com/git/git-tutorial.html")
    print(urls)
    for url in urls:
        getContentDiv(url)
        print('完成 ' + url)
        time.sleep(1)

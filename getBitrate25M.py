# coding=utf-8
import subprocess
import re
import sys, os


# 获得视频bitrate比特率信息,用于产出低于25M视频
# 获取视频的 时长 长 宽
def get_video_bitrate(path):
    process = subprocess.Popen(['ffmpeg', '-i', path], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    stdout, stderr = process.communicate()
    print(stdout.decode('utf-8'))
    pattern_bitrate = re.compile("(\d+)\s*kb\/s")
    matches = re.findall(pattern_bitrate, stdout.decode('utf-8'))
    print(matches)
    return matches


tag_size = 25000000  # 25M
path = sys.argv[1]
print(path)
print('*' * 50)
print(get_video_bitrate(path))
video_bitrate = int(get_video_bitrate(path)[1])
audio_bitrate = int(get_video_bitrate(path)[-1])
filesize = os.path.getsize(path)
compress_bitrate = tag_size * (video_bitrate + audio_bitrate) / filesize - audio_bitrate
print('小丸工具箱 2Pass 压缩的比特率为：' + str(int(compress_bitrate)))
print('获得了要压缩的比特率,\n》》》》》然后在用小丸工具箱2Pass,用该码率进行压制')

# coding=utf-8
import datetime
import cv2 as cv

def video_demo():
    # 0是代表摄像头编号，只有一个的话默认为0
    capture = cv.VideoCapture(0)
    # 获取捕获的分辨率
    width, height = capture.get(3), capture.get(4)
    print(width, height)
    # 以原分辨率的一倍来捕获
    capture.set(cv.CAP_PROP_FRAME_WIDTH, width * 2)
    capture.set(cv.CAP_PROP_FRAME_HEIGHT, height * 2)
    # 创建VideoWriter类对象
    print("提示：正在录像 请按q键结束保存、s保存图片")
    while (capture.isOpened()): # open只有相机时 如果播放video替换 True
        # 调用摄像机  # 获取一帧
        ref, frame = capture.read()
        # 将这帧转换为灰度图
        #grayframe = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
        # 输出图像,第一个为窗口名字
        cv.imshow("camre", frame)
        # 10s显示图像，若过程中按“Esc”退出,若按“s”保存照片并推出
        c = cv.waitKey(10) & 0xff
        if c == 27:  # Ese键
            # 简单暴力释放所有窗口
            capture.release()  # 关闭相机
            cv.destroyAllWindows()
            print("关闭相机")
            break
        elif c == ord('q'):
            capture.release()  # 关闭相机
            cv.destroyAllWindows()  # 关闭窗口
            print("关闭相机")
            break
        elif c == ord('s'):
            # 储存照片
            print("保存图片")
            cv.imwrite(getName() + '.png', frame)

def getName():
    # 更改文件名字为当前时间
    video_old_name = datetime.datetime.now()
    video_name = str(video_old_name).replace(':', '-')
    video_name = video_name.split('.')[0]
    return video_name


if __name__ == '__main__':
    cv.waitKey()
    video_demo()

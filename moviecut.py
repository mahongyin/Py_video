from moviepy.editor import *

# movie简单使用
from moviepy.video.fx.margin import margin
from moviepy.video.fx.mirror_x import mirror_x
from moviepy.video.fx.mirror_y import mirror_y
from moviepy.video.fx.resize import resize


def fundo():
    video = VideoFileClip("E:\\PyProjects\\test.mp4")
    # 1导出音频
    audio2 = video.audio
    audio2.write_audiofile('download\\out_audio.mp3')
    # 2剪切 5-10s
    video2 = video.subclip(5, 10)
    video2.write_videofile('download\\clip_video.mp4')
    # 3导出GIF
    video2.write_gif('download\\conv_gif.gif')
    # 4视频合并拼接
    video3 = concatenate_videoclips([video, video2])
    video3.write_videofile('download\\mgr_video.mp4')
    # 4叠加4分屏
    video4 = video.fx(margin, 10)  # 加外边距
    video5 = video4.fx(mirror_x)  # x轴镜像
    video6 = video5.fx(mirror_y)  # y轴镜像
    video7 = video6.fx(resize, 0.8)  # 缩放
    video8 = clips_array([video4, video5, video6, video7])  # 报错

    audio = AudioFileClip("E:\\PyProjects\\test.mp4")  # 提取原音频
    video8.set_audio(audio)  # 添加音频
    video8.resize(width=480).write_videofile('download\\add_video.mp4')  # 输出合并
    print('finish!!!')


if __name__ == '__main__':
    print('start!!!')
    fundo()

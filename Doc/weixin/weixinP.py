# coding:utf-8
import json
import random
import time
from time import sleep

import requests
import yaml
from selenium import webdriver


# 模拟登录获取token
def weChat_login(user, password):
    post = {}
    browser = webdriver.Chrome()
    browser.get('https://mp.weixin.qq.com/')
    sleep(3)
    browser.delete_all_cookies()
    sleep(2)
    # 点击切换到账号密码输入
    browser.find_element_by_xpath("//a[@class='login__type__container__select-type']").click()
    sleep(2)
    # 模拟用户点击
    input_user = browser.find_element_by_xpath("//input[@name='account']")
    input_user.send_keys(user)
    input_password = browser.find_element_by_xpath("//input[@name='password']")
    input_password.send_keys(password)
    sleep(2)
    # 点击登录
    browser.find_element_by_xpath("//a[@class='btn_login']").click()
    sleep(2)
    # 微信登录验证
    print('请扫描二维码')
    sleep(20)
    # 刷新当前网页
    browser.get('https://mp.weixin.qq.com/')
    sleep(5)
    # 获取当前网页链接
    url = browser.current_url
    # 获取当前cookie
    cookies = browser.get_cookies()
    for item in cookies:
        post[item['name']] = item['value']
    # 转换为字符串
    cookie_str = json.dumps(post)
    # 存储到本地
    with open('cookie.txt', 'w+', encoding='utf-8') as f:
        f.write(cookie_str)
    print('cookie保存到本地成功')
    # 对当前网页链接进行切片，获取到token
    paramList = url.strip().split('?')[1].split('&')
    # 定义一个字典存储数据
    paramdict = {}
    for item in paramList:
        paramdict[item.split('=')[0]] = item.split('=')[1]
    # 返回token
    return paramdict['token']


def doWeixin():
    token = weChat_login("15732164757", "")
    url = 'https://mp.weixin.qq.com'
    headers = {
        'HOST': 'mp.weixin.qq.com',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.183 Safari/537.36 Edg/86.0.622.63'
    }
    with open('cookie.txt', 'r', encoding='utf-8') as f:
        cookie = f.read()
    cookies = json.loads(cookie)
    resp = requests.get(url=url, headers=headers, cookies=cookies)
    search_url = 'https://mp.weixin.qq.com/cgi-bin/searchbiz?'
    params = {
        'action': 'search_biz',
        'begin': '0',
        'count': '5',
        'query': '搜索的公众号名称',
        'token': token,
        'lang': 'zh_CN',
        'f': 'json',
        'ajax': '1'
    }
    search_resp = requests.get(url=search_url, cookies=cookies, headers=headers, params=params)
    lists = search_resp.json().get('list')[0]
    # 通过上面的代码即可获取到对应的公众号数据

    fakeid = lists.get('fakeid')
    # 通过上面的代码就可以得到对应的fakeid

    appmsg_url = 'https://mp.weixin.qq.com/cgi-bin/appmsg?'
    params_data = {
        'action': 'list_ex',
        'begin': '0',
        'count': '5',
        'fakeid': fakeid,
        'type': '9',
        'query': '',
        'token': token,
        'lang': 'zh_CN',
        'f': 'json',
        'ajax': '1'
    }
    appmsg_resp = requests.get(url=appmsg_url, cookies=cookies, headers=headers, params=params_data)


# 读取cookie和user_agent
def readWeixin():
    with open("wechat.yaml", "r") as file:
        file_data = file.read()
    config = yaml.safe_load(file_data)

    headers = {
        "Cookie": config['cookie'],
        "User-Agent": config['user_agent']
    }

    # 请求参数
    url = "https://mp.weixin.qq.com/cgi-bin/appmsg"
    begin = "0"
    params = {
        "action": "list_ex",
        "begin": begin,
        "count": "5",
        "fakeid": config['fakeid'],
        "type": "9",
        "token": config['token'],
        "lang": "zh_CN",
        "f": "json",
        "ajax": "1"
    }

    # 存放结果
    app_msg_list = []
    # 在不知道公众号有多少文章的情况下，使用while语句
    # 也方便重新运行时设置页数
    i = 0
    while True:
        begin = i * 5
        params["begin"] = str(begin)
        # 随机暂停几秒，避免过快的请求导致过快的被查到
        time.sleep(random.randint(1, 10))
        resp = requests.get(url, headers=headers, params=params, verify=False)
        # 微信流量控制, 退出
        if resp.json()['base_resp']['ret'] == 200013:
            print("frequencey control, stop at {}".format(str(begin)))
            break

        # 如果返回的内容中为空则结束
        if len(resp.json()['app_msg_list']) == 0:
            print("all ariticle parsed")
            break

        app_msg_list.append(resp.json())
        # 翻页
        i += 1

if __name__ == '__main__':
    readWeixin()
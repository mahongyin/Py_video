# coding:utf-8
import os
import time

import requests
from bs4 import BeautifulSoup

from Doc.download.DownLoad import download

headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/103.0.5060.134 Safari/537.36 Edg/103.0.1264.71",
}

video_urls = []
tab1_list = []
tab2_list = []
tab3_list = []


def getTab(huya_url):
    tab1Calss = 'vhy-list-category-list'
    tab2Class = 'vhy-list-category-subs'
    tab3Class = 'vhy-videosort-tab'  # 最热视频最新发布最多播放
    response = requests.get(huya_url, headers=headers)  # 获取网页源代码
    soup = BeautifulSoup(response.text, "html.parser")
    a1_list = soup.find('div', class_=tab1Calss).find_all('a')
    for a1 in a1_list:
        tab1 = {'name': a1.text, 'url': 'https:' + a1.get('href')}
        tab1_list.append(tab1)
    a2_list = soup.find('div', class_=tab2Class).find_all('a')
    for a2 in a2_list:
        tab2 = {'name': a2.text, 'url': 'https:' + a2.get('href')}
        tab2_list.append(tab2)
    li3_list = soup.find('ul', class_=tab3Class).find_all('li')
    for li3 in li3_list:
        tab3 = {'name': li3.find('a').text, 'url': 'https:' + li3.find('a').get('href')}
        tab3_list.append(tab3)
    print("{}\n{}\n{}".format(tab1_list, tab2_list, tab3_list))


def GetUrl(base_url, start, nums):
    for page in range(start, nums):  # 迭代 10 到 20 之间的数字
        # for page in range(nums):  # 迭代 0 到 nums 之间的数字
        base_url = base_url + "&order=hot&page={}".format(page)  # order=new  page第几页
        response = requests.get(base_url, headers=headers)  # 获取网页源代码
        soup = BeautifulSoup(response.text, "html.parser")
        li_list = soup.find('ul', class_="vhy-video-list clearfix").find_all('li')
        video_urls.extend(['https://v.huya.com' + i.find('a').get("href") for i in li_list])
    print(video_urls)
    return video_urls


def SaveVideo(url, name):
    if not os.path.isdir("../videos"):
        os.makedirs("../videos")
    video_path = './videos/{}.mp4'.format(name)
    download(url, video_path)
    print("{}视已经抓取完毕".format(name))


def Getvideos(url_list):
    base_url = "https://liveapi.huya.com/moment/getMomentContent?&videoId="  # 构造json数据地址
    for url in url_list:
        videoid = url.split("/")[-1].split('.')[0]
        response = requests.get(base_url + videoid, headers=headers)
        json_data = response.json()
        video_url = json_data['data']['moment']['videoInfo']['definitions'][0]['url']  # 提取视频链接地址
        SaveVideo(video_url, videoid)  # 保存视频
        time.sleep(1)


if __name__ == '__main__2':
    # print(random.randint(1, 10))  # 产生 1 到 10 的一个整数型随机数
    # print(random.random())  # 产生 0 到 1 之间的随机浮点数
    # print(random.uniform(1.1, 5.4))  # 产生  1.1 到 5.4 之间的随机浮点数，区间可以不是整数
    # print(random.choice('tomorrow'))  # 从序列中随机选取一个元素
    # print(random.randrange(1, 100, 2))  # 生成从1到100的间隔为2的随机整数
    # GetUrl("https://v.huya.com/g/Dance?set_id=51", 0, 3)
    # Getvideos(video_urls)
    huya_url = 'https://v.huya.com/g/all?set_id=0'
    getTab(huya_url)

if __name__ == '__main__':
    GetUrl('https://v.huya.com/g/finance?set_id=63', 1, 2)
    Getvideos(video_urls)

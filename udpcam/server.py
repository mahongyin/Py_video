# coding=utf-8

import struct
from socket import (socket, AF_INET, SOCK_DGRAM, SOL_SOCKET, SO_BROADCAST)
from threading import Thread
from time import sleep
from tkinter import Tk, BooleanVar, Button, Label
from zlib import compress

import numpy
from PIL import ImageGrab

root = Tk()
root.title('屏幕广播发送')
# 初始尺寸和位置
root.geometry('320x80+500+200')
# 两个方向都不允许缩放
root.resizable(False, False)

# 缓存区大小
BUFFER_SIZZE = 60 * 1024
# 控制是否正在发送的变量
sending = BooleanVar(root, value=False)

# 获取本地IP
def get_local_ip(ifname):
    s = socket(AF_INET, SOCK_DGRAM)
    try:
        s.connect(('8.8.8.8', 80))
        ip = s.getsockname()[0]
    finally:
        s.close()
    return ip


def lastIndex(str1, c):
    target = 0
    icount = 0
    for char in str1:
        if char == c:
            target = icount
        icount = icount + 1
    return target


def send_image():
    # UDP
    sock = socket(AF_INET, SOCK_DGRAM)
    # 广播
    sock.setsockopt(SOL_SOCKET, SO_BROADCAST, 1)
    network = '<broadcast>'
    ip = get_local_ip("eth0")
    print(lastIndex(ip, '.'))
    IP = ip[:lastIndex(ip, '.')] + ".255"  # '192.168.199.255'
    # IP =network #'255.255.255.255'
    print(IP)
    lbCopyRight['text'] = '屏幕广播:' + IP
    PORT = 22222
    while sending.get():
        # 屏幕截图
        im = ImageGrab.grab()
        nbyte = numpy.asarray(im)  # im.tobytes() 花屏
        # byte2 = BytesIO(nbyte.tobytes())
        # byte3 = byte2.getvalue()
        byte3 = nbyte.tobytes()
        # 把图压缩后传输 减少带宽占用
        im_bytes = compress(byte3)
        w, h = im.size
        # image_data = decompress(im_bytes)
        # img = frombytes('RGB', [w, h], image_data)
        # img.save(r'save.png')
        # 定义头文件
        leng = len(im_bytes)
        fhead = struct.pack("lii", leng, w, h)  # fmt-> l =long, i =int, s =char[]
        sock.sendto(fhead, (IP, PORT))
        # 分块 发送 ，以免字节太长
        for i in range(leng // BUFFER_SIZZE + 1):
            if BUFFER_SIZZE * (i + 1) > leng:
                sock.sendto(im_bytes[BUFFER_SIZZE * i:], (IP, PORT))
            else:
                sock.sendto(im_bytes[BUFFER_SIZZE * i:BUFFER_SIZZE * (i + 1)], (IP, PORT))
            # start = i * BUFFER_SIZZE
            # end = start + BUFFER_SIZZE
            # sock.sendto(im_bytes[start:end], (IP, PORT))
        print('发送屏幕截图' + str(leng))
        sleep(0.5)
    sock.close()


lbCopyRight = Label(root, text='屏幕广播', fg='red', cursor='plus')
lbCopyRight.place(x=5, y=5, width=310, height=20)


# url = r'https://mp.weixin.qq.com/s/'
# lbCopyRight.bind("<Button-1>", lambda e: startfile(url))


def btnStartClick():
    sending.set(True)
    Thread(target=send_image).start()
    btnStart['state'] = 'disabled'
    btnStop['state'] = 'normal'


btnStart = Button(root, text='开始广播', command=btnStartClick)
btnStart.place(x=30, y=40, width=125, height=20)


def btnStopClick():
    sending.set(False)
    btnStart['state'] = 'normal'
    btnStop['state'] = 'disabled'


btnStop = Button(root, text='停止广播', command=btnStopClick)
btnStop['state'] = 'disabled'
btnStop.place(x=165, y=40, width=125, height=20)

if __name__ == '__main__':
    root.mainloop()

import socketserver


class MyUDPHandler(socketserver.BaseRequestHandler):

    def handle(self):
        print("{}\t{}".format(self.client_address[0], str(self.request[0].strip(), 'utf-8')))


if __name__ == "__main__":
    HOST, PORT = "0.0.0.0", 9696

    server = socketserver.UDPServer((HOST, PORT), MyUDPHandler)

    server.serve_forever()

import requests
from bs4 import BeautifulSoup
from fake_useragent import UserAgent

ua = UserAgent()
headers = {'User-Agent': ua.random}  # 请求头

# 1 构造网址
url = 'https://www.jianshu.com/'

# 2 请求网址
html = requests.get(url, headers=headers)  # 发起请求 简书需要添加headers

# 3 构建鸡汤
soup = BeautifulSoup(html.text, 'lxml')  # 获取文章信息，用BeautifulSoup转成html

# 4 提取数据
li_list = soup.find('ul', class_='note-list').find_all('li')
data = {}
data_list = []

for li in li_list:  # 循环获取每一篇文章内容信息
    title = li.find('a', class_='title').text.strip()
    title_id = li.find('a', class_='title')['href'].split('/')[-1]
    abstract = li.find('p', class_='abstract').text.strip()
    article_url = 'https://www.jianshu.com' + li.find('a', class_='title')['href']
    author = li.find('a', class_='nickname').text.strip()
    author_id = li.find('a', class_='nickname')['href'].split('/')[-1]
    author_url = 'https://www.jianshu.com' + li.find('a', class_='nickname')['href']
    comment_num = li.find('div', class_='meta').find_all('a')[1].text.strip()
    comment_url = 'https://www.jianshu.com' + li.find('div', class_='meta').find_all('a')[1]['href']
    like_num = li.find('div', class_='meta').find('span').text.strip()

    data['标题id'] = title_id
    data['标题'] = title
    data['介绍'] = abstract
    data['文章网址'] = article_url
    data['作者id'] = author_id
    data['作者'] = author
    data['作者网址'] = author_url
    data['评论数量'] = comment_num
    data['评论网址'] = comment_url
    data['喜欢'] = like_num
    data_list.append(data)

print(data_list)
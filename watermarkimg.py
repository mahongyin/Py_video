# -*- coding: utf-8 -*-

import moviepy.editor as mp

#本地视频位置
video = mp.VideoFileClip("E:/PyProjects/test.mp4")

#准备log图片
logo = (mp.ImageClip("E:/PyProjects/lena.png")
        .set_duration(video.duration) # 水印持续时间
        .resize(height=100) # 水印的高度，会等比缩放
        .margin(right=8, top=8, opacity=1) # 水印边距和透明度
        .set_pos(("left","center"))) # 水印的位置

final = mp.CompositeVideoClip([video, logo])
# mp4文件默认用libx264编码， 比特率单位bps
final.write_videofile("test.mp4", codec="libx264", bitrate="10000000")
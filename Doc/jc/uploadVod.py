# coding:utf-8
import json
import os
import time

import requests
from requests_toolbelt import MultipartEncoder
from requests_toolbelt.multipart import encoder
from Doc.download.DownLoad import upload


def my_callback(monitor):
    progress = (monitor.bytes_read / monitor.len) * 100
    print("\r 文件上传进度：%d%%(%d/%d)"
          % (progress, monitor.bytes_read, monitor.len), end=" ")
    pass


# 读取文件夹 removeText把该内容替换为空
def dfs(dir_name, cookie2, xtoken2, removeText, content2):
    # os.listdir()方法获取文件夹名字，返回数组
    file_name_list = os.listdir(dir_name)
    # 转为转为字符串
    # file_name = str(file_name_list)
    for name in file_name_list:
        if name.endswith(".mp4"):
            res = uploadimg(cookie2, xtoken2, dir_name + "/" + name)
            if res.status_code == 200:
                print('上传成功' if res.text == '' else eval("u" + "\'" + res.text + "\'"))
                dataResult = json.loads(res.text)['data']
                id = dataResult['id']
                salt = dataResult['salt']
                if len(removeText) > 0:
                    name = name.replace(removeText, "")
                print(name)
                content = '{}，{}'.format("本节视频，我们将学习 "+name[:-4], content2)
                putBlogs(cookie2, xtoken2, name, content, id)
            else:
                print('上传失败' if res.text == '' else eval("u" + "\'" + res.text + "\'"))
            time.sleep(1)


def putBlogs(cookie2, xtoken2, title, content, id):
    aheaders = {'Content-Type': 'application/json; charset=UTF-8',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
                'Cookie': cookie2,
                'x-token': xtoken2,
                'Referer': 'http://jeacher.com'
                }
    blogurl = 'http://jeacher.com/content/release/video'  # 'http://jeacher.com/content/draft/video'
    adata = {"title": title,
             "content": content,
             "id": id}
    response = requests.post(blogurl, headers=aheaders, json=adata)
    print(response.text)


def uploadimg(cookie, xtoken, img_path):
    url1 = 'http://jeacher.com/content/video/upload'
    # from_data上传文件，注意参数名uploadFile
    filename, suffix = os.path.splitext(img_path)
    fname = str(filename).split("/").pop() + suffix
    opf = open(img_path, 'rb')
    # data = MultipartEncoder(
    #     fields={'suffix': suffix, 'uploadFile': (fname, opf, 'video/*')}
    # )
    edata = encoder.MultipartEncoder(
        fields={'suffix': suffix, 'uploadFile': (fname, opf, 'video/*')}
    )
    # mdata = encoder.MultipartEncoderMonitor(edata, my_callback)
    aheaders = {'Content-Type': edata.content_type,  # data.content_type
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
                'Cookie': cookie,
                'x-token': xtoken,
                'Referer': 'http://jeacher.com'}
    # res0 = requests.post(url=url1, data=data, headers=aheaders)
    res0 = upload(url1, aheaders, edata)  # requests.post(url=url1, data=mdata, headers=aheaders)
    # {"message":"success","code":0,"data":{"id":"1661576289501uu4mxt2k99","title":null,"content":null,"leaderId":"1660659333237qxuvezi5dy","contentUrl":"http://jeacher.oss-cn-beijing-internal.aliyuncs.com/video/1661576289501uu4mxt2k99.mp4","posterUrl":"http://jeacher-assets.oss-cn-beijing.aliyuncs.com/poster/1661576289501uu4mxt2k99.jpg","blogsHtml":null,"releaseTime":null,"createTime":null,"contentType":"1","status":"1","salt":"1661576289501h6d0lbv1z5","videoIsDeal":null,"videoLength":1923540,"weight":null},"requestId":null}
    opf.close()
    return res0


if __name__ == '__main__':
    cookie2 = 'token=3214f652e3b7d26da36bf37073421496de484371668c4676fdd6fc94afa658ff26c178dd8831110c5f1816975de69f215c618f82d027e58dd85bb520c24c5294f5a27aa69fff336d1be11b1d09d77efead413dbd0cdfbe9be39ea7680dde895960b01d87b70d3a2148f036e7581a921dfec61559c58244eebc913ec628472ba8f465fe4620d8017bf19c9d11f3cb88470cdbbf6c874fa08c4b3540ea996a94cbde35a40dbaa2813e16ab2ebe719e19ab; phone=10241024000; loginAccount=10241024000'
    xtoken2 = '3214f652e3b7d26da36bf37073421496de484371668c4676fdd6fc94afa658ff26c178dd8831110c5f1816975de69f215c618f82d027e58dd85bb520c24c5294f5a27aa69fff336d1be11b1d09d77efead413dbd0cdfbe9be39ea7680dde895960b01d87b70d3a2148f036e7581a921dfec61559c58244eebc913ec628472ba8f465fe4620d8017bf19c9d11f3cb88470cdbbf6c874fa08c4b3540ea996a94cbde35a40dbaa2813e16ab2ebe719e19ab'
    # path定义要获取的文件名称的目录
    path = "E:\BaiduNetdiskDownload\积书视频素材\lucene"
    index2 = """Lucene是一套用于全文检索和搜寻的开源程式库，由Apache软件基金会支持和提供。
    Lucene提供了一个简单却强大的应用程式接口，能够做全文索引和搜寻。在Java开发环境里Lucene是一个成熟的免费开源工具。
    就其本身而言，Lucene是当前以及最近几年最受欢迎的免费Java信息检索程序库。"""
    # 本篇是在进行大数据相关课程学习后归类的Hadoop学习教程，希望能给学习Hadoop同学一些启发！
    #                    Hadoop 是一个用于大数据的架构解决方案。经过多年的开发演进，Hadoop 已成为一个庞大的系统，它的内部工作机制非常复杂，
    #                    是一个结合了分布式理论与具体的工程开发的整体架构。
    dfs(path, cookie2, xtoken2, "", index2)

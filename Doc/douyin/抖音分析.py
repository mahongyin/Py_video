from appium import webdriver
from selenium.webdriver.common.by import By
from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import threading
import pymysql
import traceback
import cv2
import numpy as np
import matplotlib.pyplot as plt
from selenium.webdriver.common.keys import Keys
# 分析年龄性别
def getAppStatus(app_name,password):
    '''
    需要app_name,password同时满足才可以调用，如果后期要关闭
    :param app_name:
    :param password:
    :return:
    '''
    conn = pymysql.connect(host="112.74.28.136", port=3306, user="app_account_control", passwd="Ys666888$$$", db="software", charset="utf8")
    try:
        cursor = conn.cursor()
        cursor.execute('''select * from app_account_control where app_name="{}" and pswd="{}"'''.format(app_name, password))
        oneRecord = cursor.fetchone()
        if(oneRecord!=None):
            return True
        else:
            return False
    except:
        cursor.close()
        conn.rollback()
    finally:
        cursor.close()
        conn.close()

def conn_sql():
    db_arr = []
    with open('./db_info.txt', 'r', encoding='utf-8') as f:
        lines = f.readlines()
        for li in lines:
            db_arr.append(li.strip().split('=')[1])
    conn = pymysql.connect(host=db_arr[0], port=int(db_arr[1]), user=db_arr[2], password=db_arr[3], db=db_arr[4])
    cursor = conn.cursor()

    return conn, cursor

# 配置uuid 和ip
def ip_config():
    arr = []
    with open('./ip.txt', 'r', encoding='utf-8') as f:
        line = f.readlines()
        for li in line:
            arr.append(li.strip().split('---'))

    dic = {}
    for a in arr:
        dic[a[0]] = a[1]

    return dic

def by_id_ele(driver, xpath, t=10):
    count = 0
    while count < 3:
        try:
            # ele = WebDriverWait(driver, t).until(EC.presence_of_element_located((By.ANDROID_UIAUTOMATOR, xpath)))
            ele = WebDriverWait(driver, t).until(EC.presence_of_element_located((By.ID, xpath)))
            return ele
        except:
            count += 1

def by_text_ele(driver, xpath, t=30):
    count = 0
    while count < 3:
        try:
            ele = WebDriverWait(driver, t).until(EC.presence_of_element_located((By.ANDROID_UIAUTOMATOR, xpath)))
            return ele
        except:
            count += 1

def run(server, udid):
    global no, apps_dic
    try:
        apps_dic[udid] = False
        # print(udid, '启动')
        # print(server, '开始')
        desired_caps = {
            "platformName": "Android",
            "deviceName": "HUAWEI",
            "appPackage": "com.ss.android.ugc.aweme",
            "appActivity": ".splash.SplashActivity",
            "newCommandTimeout": 600000,
            "udid": udid,
            "autoGrantPermissions": True,
            "noReset": True
        }
        # appium服务监听地址
        # server = 'http://localhost:4723/wd/hub'
        # 驱动
        driver = webdriver.Remote(server, desired_caps)
        # 等主页面activity出现
        driver.wait_activity(".splash.SplashActivity", 10)
        time.sleep(3)
        print('开始执行')
        try:
            by_id_ele(driver, 'com.ss.android.ugc.aweme:id/evn', 20).click()
        except:
            by_id_ele(driver, 'com.ss.android.ugc.aweme:id/e1f', 20).click()

        while select_user():
            # 查找
            search(driver, select_user())
    except:
        # traceback.print_exc()
        pass
    finally:
        # 释放
        apps_dic[udid] = server

def search(driver, user_id):
    # 点击搜索输入好友
    try:
        search_text = by_id_ele(driver, "com.ss.android.ugc.aweme:id/et_search_kw")
        search_text.send_keys('1')
        close_btn = by_id_ele(driver, "com.ss.android.ugc.aweme:id/btn_clear")
        # search_text.send_keys(Keys.CONTROL, 'a')
        close_btn.click()
        search_text.send_keys(user_id)
        try:
            by_text_ele(driver, 'new UiSelector().text("搜索")').click()
            time.sleep(2)
            by_text_ele(driver, 'new UiSelector().text("用户")').click()
            # 默认点击第一个 搜索最接近
            # driver.find_element_by_class_name('com.lynx.tasm.behavior.ui.UIBody').click()
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CLASS_NAME, 'com.lynx.tasm.behavior.ui.UIBody'))).click()
            time.sleep(2)
            # 截图
            try:
                WebDriverWait(driver, 7).until(EC.presence_of_element_located((By.XPATH, '//*[@resource-id="com.ss.android.ugc.aweme:id/kks"]/*[@class="android.widget.TextView"][1]'))).screenshot('./img/sex.png')
                # driver.find_element_by_xpath('//*[@resource-id="com.ss.android.ugc.aweme:id/kks"]/*[@class="android.widget.TextView"][1]').screenshot('./img/sex.png')
                if check_img('man'):
                    gender ='男'
                    # print('man')
                elif check_img('girl'):
                    gender = '女'
                    # print('girl')
                else:
                    gender = '未知'
                    # print('未知')
            except:
                gender = '未知'

            # 获取年龄
            try:
                age = WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.ID, 'com.ss.android.ugc.aweme:id/kks'))).find_elements_by_class_name('android.widget.TextView')[0].text
                # driver.find_element_by_id('com.ss.android.ugc.aweme:id/kks').find_elements_by_class_name('android.widget.TextView')[0].text
                if '岁' in age:
                    age = age.replace('岁', '')
                else:
                    age = 0
            except:
                age = 0

            conn, cursor =conn_sql()
            sql = 'UPDATE users SET age=%s, gender=%s WHERE userId =%s '
            cursor.execute(sql, (int(age), gender, user_id))
            conn.commit()
        except:
            # traceback.print_exc()
            pass
        finally:
            driver.keyevent(4)
    except:
        # traceback.print_exc()
        pass

def select_user():
    try:
        conn, cursor = conn_sql()
        sql ="SELECT userId FROM `users` WHERE gender ='' LIMIT 1"
        cursor.execute(sql)
        cf = cursor.fetchall()
    except:
        # traceback.print_exc()
        pass
    finally:
        if len(cf) > 0:
            return cf[0]
        else:
            return False


def check_img(sex):
    scale = 1

    img = cv2.imread('./img/sex.png', 1)#要找的大图
    img = cv2.resize(img, (0, 0), fx=scale, fy=scale)

    template = cv2.imread(f'./img/{sex}.png', 1)#图中的小图
    try:
        template = cv2.resize(template, (0, 0), fx=scale, fy=scale)
        template_size= template.shape[:2]

    #找图 返回最近似的点
    # def search_returnPoint(img,template,template_size):
        img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        template_ = cv2.cvtColor(template,cv2.COLOR_BGR2GRAY)
        result = cv2.matchTemplate(img_gray, template_,cv2.TM_CCOEFF_NORMED)
        threshold = 0.7
        # res大于70%
        loc = np.where(result >= threshold)
        # 使用灰度图像中的坐标对原始RGB图像进行标记
        point = ()
        for pt in zip(*loc[::-1]):
            cv2.rectangle(img, pt, (pt[0] + template_size[1], pt[1] + + template_size[0]), (7, 249, 151), 2)
            point = pt
        if point==():
            # pass
            img =None
        # return img,point[0]+ template_size[1] /2,point[1]

    # img,x_,y_ = search_returnPoint(img,template,template_size)
    #     img,x_,y_ = check_img('girl')
        if(img is None):
            # print("没找到图片")
            return False
        else:
            # print("找到图片 位置:"+str(x_)+" " +str(y_))
            plt.figure()
            plt.imshow(img, animated=True)
            # plt.show()
            # print(sex)
            return True
    except:
        # print('未知')
        return False


if __name__ == '__main__':
    if getAppStatus(app_name="douyin1104", password="ae8b5aa26a3ae31612eec1d1f6ffbce9"):
        print("本程序只是辅助自动办公，如数据有权限要求，请与数据提供商取得账号授权, 并用于合法用途")
        apps_dic = ip_config()
        while True:
            # 没有数据60s后重新
            if not select_user():
                print('后台没有可以需要更新性别年龄的用户,5分钟后程序自动再次尝试')
                time.sleep(500)

            else:
                for ad in apps_dic.items():
                    if ad[1]:
                        run(ad[1], ad[0])

import SocketServer



class MyUDPHandler(SocketServer.BaseRequestHandler):

    def handle(self):

        print "{}\t{}".format(self.client_address[0], self.request[0].strip())



if __name__ == "__main__":

    HOST, PORT = "0.0.0.0", 9696

    server = SocketServer.UDPServer((HOST, PORT), MyUDPHandler)

    server.serve_forever()
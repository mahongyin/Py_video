# coding:utf-8
import json
import requests
import time

has_more = True


def get_data(url, info, headers):
    global has_more
    resp = requests.post(url, json=info, headers=headers)  # data自动使用json方式发送
    data = json.loads(resp.text)
    info["cursor"] = data["cursor"]
    has_more = data["has_more"]
    for i in data["data"]:
        try:  # 去除广告推广内容的报错
            print("作者：" + i["item_info"]["author_user_info"]["user_name"], end="————")
            print("标题：" + i["item_info"]["article_info"]["title"])
            print("地址：" + i["item_info"]["article_info"]["url"])
        except:
            continue
    time.sleep(4)


if __name__ == "__main__":

    url = "https://api.juejin.cn/recommend_api/v1/article/recommend_all_feed"
    info = {"id_type": 2, "client_type": 2608, "sort_type": 200, "cursor": "0", "limit": 20}
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36',
    }

    while has_more:
        get_data(url, info, headers)

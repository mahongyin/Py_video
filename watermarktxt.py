# -*- coding: utf-8 -*-
from moviepy.editor import *

# 本地视频位置
from moviepy.video.VideoClip import TextClip
# subclip视频截取开始时间和结束时间
video = VideoFileClip("E:/PyProjects/test.mp4").subclip(0,100)

# 制作文字，指定文字大小和颜色
txt_clip = (TextClip("文字水印", fontsize=70, color='red')
             .set_position('center')#水印内容居中
             .set_duration(100) )#水印持续时间

result = CompositeVideoClip([video, txt_clip]) #在视频上覆盖文本
result.write_videofile("myHolidays_edited.mp4", fps=25)#fps:视频文件中每秒的帧数
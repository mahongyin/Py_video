# coding:utf-8
import json
import os
import re
import time

import requests
from bs4 import BeautifulSoup

from itertools import product
from PIL import Image

# https://download.csdn.net/download  https://blog.csdn.net/
# <img src="https://img-blog.csdnimg.cn/20210403130143468.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzQyNDEzMDEx,size_16,color_FFFFFF,t_70" alt="在这里插入图片描述">
# <pre data-index="5" class="prettyprint"><code
from requests_toolbelt import MultipartEncoder


def upload(cookie, xtoken, title, img):
    title = re.sub('[^\w\-_\. ]', '', title)
    # print(title)
    name = download_img(title, img)
    if len(name) > 0:
        # t = time.time()
        # timestamp = int(round(t * 1000))  # 13位
        res = uploadimg(cookie, xtoken, name)
        # 'application/octet-stream'
        if res.status_code == 200:
            print('上传成功' if res.text == '' else eval("u" + "\'" + res.text + "\'"))
            return json.loads(res.text)['data']
        # {"message":"success","code":0,"data":"http://jeacher-assets.oss-cn-beijing.aliyuncs.com/blogs/1659868810227dwe5w87uzi.png","requestId":null}
        print('上传失败' if res.text == '' else eval("u" + "\'" + res.text + "\'"))
        return ""
    else:
        return ""


# suffix  .png
def uploadimg(cookie, xtoken, img_path):
    url1 = 'http://jeacher.com/content/blogs/image/upload'
    # from_data上传文件，注意参数名uploadFile
    # t = time.time()
    # timestamp = int(round(t * 100000))  # 15位 str(timestamp) + suffix
    filename, suffix = os.path.splitext(img_path)
    fname = str(filename).split("/").pop() + suffix
    opf = open(img_path, 'rb')
    data = MultipartEncoder(
        fields={'suffix': suffix, 'uploadFile': (fname, opf, 'image/*')})
    # fields={'suffix': suffix, 'uploadFile': ('filename', open(img_path, 'rb'), 'image/*')})
    aheaders = {'Content-Type': data.content_type,
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
                'Cookie': cookie,
                'x-token': xtoken,
                'Referer': 'http://jeacher.com/jeacher/blogsEditor/blogsEditor.html'}
    res0 = requests.post(url=url1, data=data, headers=aheaders)
    opf.close()
    return res0


def download_img(title, img_url):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36'
    }
    r = requests.get(img_url, headers=headers, stream=True)
    # print(r.status_code) # 返回状态码
    fileName = ''
    if r.status_code == 200:
        if not os.path.isdir("./img/" + title):
            os.makedirs("./img/" + title)
        # 截取图片文件名
        img_name = img_url.split('#')[0].split('?')[0].split('/').pop()
        if '.png' in img_name or '.jpg' in img_name or '.jpeg' in img_name or '.gif' in img_name or '.webp' in img_name:
            fileName = "./img/" + title + "/" + img_name
            with open(fileName, 'wb') as f:
                f.write(r.content)
            print("download img", fileName)
            # 去水印
            # clearWater2(fileName)
        return fileName
    else:
        return fileName


# # 去除水印
# def clearWater(imgPath):
#     img = cv2.imread(imgPath)
#     new = np.clip(1.4057577998008846 * img - 38.33089999653017, 0, 255).astype(np.uint8)
#     cv2.imwrite(imgPath, new)


def clearWater2(imgPath):
    img = Image.open(imgPath)
    width, height = img.size
    for pos in product(range(width), range(height)):
        if sum(img.getpixel(pos)[:3]) > 600:
            img.putpixel(pos, (255, 255, 255))
    img.save(imgPath)


def getContentDiv(cookie, xtoken, bgurl):
    if 'https://blog.csdn.net/' in bgurl or 'http://blog.csdn.net' in bgurl:
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36'
        }
        res = requests.get(bgurl, headers=headers)
        if res.status_code == 200:
            cdata = res.content.decode('utf8')
            # print(res.text)
            cont = BeautifulSoup(cdata, 'html.parser')
            # 获取包含文章内容的标签 attrs后跟的是最外层标签属性，根据爬取网站的实际情况进行修改 id:content  class:mainxx
            title = ''
            for htitle in cont.select('div.article-title-box'):
                title = htitle.find('h1', attrs={'id': 'articleContentId'}).text
            data = cont.find('div', attrs={'id': 'content_views'})  # CSDN
            # for table in cont.select('table.reference'):  # class="reference"
            title = title.strip()
            if len(title) > 5:
                if len(title) > 50:
                    title = title[:49]
            else:
                dt = time.strftime('%Y年%m月%d日  %H时%M分%S秒', time.localtime())
                title = title + "-" + dt
            for table in data.find_all("table"):
                table["class"] = "tableBox"
            for tr in data.find_all("tr"):
                tr["class"] = "trBox"
            tds = data.find_all("td")
            for td in tds:
                td["class"] = "tdBox"
            ths = data.find_all("th")
            for th in ths:
                th["class"] = "tdBox"
            replacement = """
            <pre class="codeBox">
            {text}
            </pre>
            """
            for precode in data.find_all('pre'):
                # precode["class"] = "codeBox"
                precode.replace_with(BeautifulSoup(replacement.format(text=precode.text), 'html.parser'))
            # 图操作
            for imgv in data.find_all('img'):
                yimg = imgv["src"]
                if 'https://img-blog.csdnimg.cn/' in yimg or 'https://imgconvert.csdnimg.cn' in yimg:
                    ititle = re.sub("[^a-zA-Z0-9\u4e00-\u9fa5]", '', title)
                    # ititle = ''.join(re.findall('[\u4e00-\u9fa5]', title))
                    ossimg = upload(cookie, xtoken, ititle, yimg)
                    if len(ossimg) > 0:
                        imgv["src"] = ossimg
                        imgv["class"] = "imgBox"
            print('*************************************************\n')
            data.prettify()

            # print(cont.find('div', class_="article-body"))
            value = cont.find('div', attrs={'id': 'content_views'})
            value2 = str(value).replace('\n', '').replace('\r', '').replace('\\ss', ' ')
            beirf = value.text[:150]
            # print(value2)
            putBlogs(cookie, xtoken, title, beirf, str(value))
    else:
        print('目前仅支持CSDN')


# status 1草稿 2发布
def putBlogs(cookie, xtoken, title, content, html):
    aheaders = {'Content-Type': 'application/json; charset=UTF-8',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
                'Cookie': cookie,
                'x-token': xtoken,
                'Referer': 'http://jeacher.com/jeacher/blogsEditor/blogsEditor.html'}
    blogurl = "http://jeacher.com/content/draft/blogs"
    # blogurl = "http://jeacher.com/content/release/blogs"
    adata = {
        'title': title,
        # 'content': content,
        'blogsHtml': html,
        # 'contentType': '2',
        # 'posterUrl': '',
        # 'status': '1',#1草稿 2发布
        'id': '',
    }
    response = requests.post(blogurl, headers=aheaders, json=adata)
    print(response.text)


if __name__ == "__main__":
    list = ['http://blog.csdn.net/madman_donghui/article/details/125515456',
            'http://blog.csdn.net/madman_donghui/article/details/108626068',
            'http://blog.csdn.net/madman_donghui/article/details/108625764',
            'http://blog.csdn.net/madman_donghui/article/details/100515065',
            ]
    list.reverse()
    cookie0 = 'phone=10000000000; token=930c7c291ade1753df2c7ba125e339ba4f3fc51fe2a0527bfc20b4976aac770085239b4936cfa8c11aec78479e6d7b485fbe6e5dd9dd4c2c661cf58ffb909ebf8cffc07dbd275548c776b80139152fc596110b5c502e2aab15fbc2e9160e95d0fa0ebc64ec28ca41bc5be57954d31f3f46e272d93bfe9bfe590b2ee888b18799e6a80ff21a2b9c4a932b484190edb62b698d505a39e275ba3aeae55b43a6309200fb2d3947c147c975e99c1cf47554ead69da67182caa521adfa304537d7f258c46a748d8c83647bd7d89f3cd791b69a3614a1d694036da7a7f7b54dcd66c37865f64a18648f4d9f411629158d94cdc87ded01c0ef154529e94b197e881f47d4a9ecf8fa20428cbeb6cc67204678c7cd8fc0d19d8548c22e6b4fa2af34e8cd1f; test=aaaa'
    xtoken0 = '930c7c291ade1753df2c7ba125e339ba4f3fc51fe2a0527bfc20b4976aac770085239b4936cfa8c11aec78479e6d7b485fbe6e5dd9dd4c2c661cf58ffb909ebf8cffc07dbd275548c776b80139152fc596110b5c502e2aab15fbc2e9160e95d0fa0ebc64ec28ca41bc5be57954d31f3f46e272d93bfe9bfe590b2ee888b18799e6a80ff21a2b9c4a932b484190edb62b698d505a39e275ba3aeae55b43a6309200fb2d3947c147c975e99c1cf47554ead69da67182caa521adfa304537d7f258c46a748d8c83647bd7d89f3cd791b69a3614a1d694036da7a7f7b54dcd66c37865f64a18648f4d9f411629158d94cdc87ded01c0ef154529e94b197e881f47d4a9ecf8fa20428cbeb6cc67204678c7cd8fc0d19d8548c22e6b4fa2af34e8cd1f'
    cookie1 = 'test=aaaa; token=619f3ccce41c10d2bc1dac9bc43b9bab674fc986e1d315adc56f466b53180dee26ace56889787eff59dc5fcdf2376a4513edb47bd60f335d76186f8a1b52d44ef0d79405d61cb43add9a71b44cba5256e2afc3e62bc25c18ffcb50c4aa82527ae77a9c90a2ca3268542ff3e9ac11f3c97180bb0ef82e6058cd1964dee217a3375bc6657338d126050a0489ec6ea307abaa715f472ad12587533ae70e37a8036b98d3fa4fdf53c1225e2071c8d02459032f198c2476b929f82ac514194e090c3658afafc2762918a10ba4a45f9cb1601884e08e29ff2cd33a273f4a550cbf2b8ba6ca72efc79ab973ed4b4f02334392eb1be8bc66e7167591b79c678d849f1313cd3af6d7aff4da65976fbe4fab790b7e02b921427886a19b2137f91cc3119e1c666cd0c0f94feb3a2c35a7d68f41eb5d; phone=10000000001'
    xtoken1 = '619f3ccce41c10d2bc1dac9bc43b9bab674fc986e1d315adc56f466b53180dee26ace56889787eff59dc5fcdf2376a4513edb47bd60f335d76186f8a1b52d44ef0d79405d61cb43add9a71b44cba5256e2afc3e62bc25c18ffcb50c4aa82527ae77a9c90a2ca3268542ff3e9ac11f3c97180bb0ef82e6058cd1964dee217a3375bc6657338d126050a0489ec6ea307abaa715f472ad12587533ae70e37a8036b98d3fa4fdf53c1225e2071c8d02459032f198c2476b929f82ac514194e090c3658afafc2762918a10ba4a45f9cb1601884e08e29ff2cd33a273f4a550cbf2b8ba6ca72efc79ab973ed4b4f02334392eb1be8bc66e7167591b79c678d849f1313cd3af6d7aff4da65976fbe4fab790b7e02b921427886a19b2137f91cc3119e1c666cd0c0f94feb3a2c35a7d68f41eb5d'
    cookie2 = 'test=aaaa; token=619f3ccce41c10d2bc1dac9bc43b9bab674fc986e1d315adc56f466b53180dee26ace56889787eff59dc5fcdf2376a4513edb47bd60f335d76186f8a1b52d44ef0d79405d61cb43add9a71b44cba5256e2afc3e62bc25c18ffcb50c4aa82527abb9ea5c1d81350fadf3777973e8850bbd88d526df033a05f366be4903c53eb715bc6657338d126050a0489ec6ea307abab609e316e9d2b740e98af900f8d728398d3fa4fdf53c1225e2071c8d0245903c6d0baba0da10d15233b3dec24ff6baf27dace63bb355b2e3815676a735a3baf49d6379e7b91f88038e1831fd7f1e47ed5d46a433b82ae2d7d9c3389730704c6837ad6150108096c30a6cf0eb359413a7da74d1b3c88f2732619f429a27ce698340f114419d0b89b3a73324d8409cb4a3769f287f3ae00939f96fc78396b9e012a26eb4b48ea69ae5e192275d2123a11071d9443e559c21745a8ff8f5c6cd969fbc497b5a3dcb7130647c61e487d94c9; phone=10000000002'
    xtoken2 = '619f3ccce41c10d2bc1dac9bc43b9bab674fc986e1d315adc56f466b53180dee26ace56889787eff59dc5fcdf2376a4513edb47bd60f335d76186f8a1b52d44ef0d79405d61cb43add9a71b44cba5256e2afc3e62bc25c18ffcb50c4aa82527abb9ea5c1d81350fadf3777973e8850bbd88d526df033a05f366be4903c53eb715bc6657338d126050a0489ec6ea307abab609e316e9d2b740e98af900f8d728398d3fa4fdf53c1225e2071c8d0245903c6d0baba0da10d15233b3dec24ff6baf27dace63bb355b2e3815676a735a3baf49d6379e7b91f88038e1831fd7f1e47ed5d46a433b82ae2d7d9c3389730704c6837ad6150108096c30a6cf0eb359413a7da74d1b3c88f2732619f429a27ce698340f114419d0b89b3a73324d8409cb4a3769f287f3ae00939f96fc78396b9e012a26eb4b48ea69ae5e192275d2123a11071d9443e559c21745a8ff8f5c6cd969fbc497b5a3dcb7130647c61e487d94c9'
    cookie3 = 'token=619f3ccce41c10d2bc1dac9bc43b9bab674fc986e1d315adc56f466b53180dee1e3788726fdc521d0a46535cfa01ca9a78d8e4713ad97e8669f1d10fb9a63bca8861b4a56ca818c1987d363a3f70c3a77c8a6d7250dd6ab0c9d7d6c0f8fe0512ec3f55e1815fb16809bfc261574a2760d435f61006e8c1e53a9fd7c5d06e0790423534adf9c5779c0aac8c86c943bd3239fa5b1a7cbaab27426d7cdc2f79d852f8391970a04efda8942df028026677fd2a5d9259627defaff0619b99b960c3b342076a68ed48f67a1a7d5d2d07afd435a392508da99b2a90efff04ee9018994ce9b2fbdf0e1ecefa887a50ab306d296ed97a4cd8aaabcfc0f48f3db2b66b445958afafc2762918a10ba4a45f9cb1601879d237b0b708ecccee6603320b69b7eaa6ca72efc79ab973ed4b4f02334392eb0eedabd38410cb35e5217b27bdb90e8c451456c385b4e5342edcb15d5f600b8ed0dffc5f8040ef7b7943220f91633b44b984d06e94fa1238f90d335ea7a4ac673614a3703d272dda0f502adc39e12db47e12ae1062eb2c3b9fa688fec4177d664d580bc171d84dbe1c1a1c465c0b4cda; phone=10241024010; loginAccount=10241024010',
    xtoken3 = '619f3ccce41c10d2bc1dac9bc43b9bab674fc986e1d315adc56f466b53180dee1e3788726fdc521d0a46535cfa01ca9a78d8e4713ad97e8669f1d10fb9a63bca8861b4a56ca818c1987d363a3f70c3a77c8a6d7250dd6ab0c9d7d6c0f8fe0512ec3f55e1815fb16809bfc261574a2760d435f61006e8c1e53a9fd7c5d06e0790423534adf9c5779c0aac8c86c943bd3239fa5b1a7cbaab27426d7cdc2f79d852f8391970a04efda8942df028026677fd2a5d9259627defaff0619b99b960c3b342076a68ed48f67a1a7d5d2d07afd435a392508da99b2a90efff04ee9018994ce9b2fbdf0e1ecefa887a50ab306d296ed97a4cd8aaabcfc0f48f3db2b66b445958afafc2762918a10ba4a45f9cb1601879d237b0b708ecccee6603320b69b7eaa6ca72efc79ab973ed4b4f02334392eb0eedabd38410cb35e5217b27bdb90e8c451456c385b4e5342edcb15d5f600b8ed0dffc5f8040ef7b7943220f91633b44b984d06e94fa1238f90d335ea7a4ac673614a3703d272dda0f502adc39e12db47e12ae1062eb2c3b9fa688fec4177d664d580bc171d84dbe1c1a1c465c0b4cda',

    for url in list:
        getContentDiv(cookie3, xtoken3, url)
        time.sleep(1)
# getContentDiv('https://blog.csdn.net/xiaowanzi_zj/article/details/121690155?ops_request_misc=&request_id=&biz_id=102&spm=1018.2226.3001.4187')

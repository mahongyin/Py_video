# coding:utf-8
import json
import os
import re
import time

import regex
import requests
from bs4 import BeautifulSoup
from requests_toolbelt import MultipartEncoder
from itertools import product
from PIL import Image


def headers():
    return {
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
        "Cookie": "DWRSESSIONID=M8REiRpofPnQ!cY2JIaJhomHaL5PKjSWVTn; _ga=GA1.1.1429948185.1647331191; Hm_lvt_c222a05e965a074b1e497d3178c74667=1647509227; Hm_lpvt_c222a05e965a074b1e497d3178c74667=1647575206; _ga_8DVJBWHFVQ=GS1.1.1647743588.7.0.1647743588.0; JSESSIONID=node01qjautu2b0drjdbx31bxb3fnk67.node0; csrf=7bnsr5V6en1Zxmi; _jpanonym=\"Y2E0MzZiNTllZGZjYWU2MTgxNjFjZjBhNjYzN2YyYmQjMTY1OTgwNzA0NzAzNyMzMTUzNjAwMCNOVEU0TnpBMU5EazBaak5qTkdKbE56aG1NVEpsTWpBNFpHVTBOamt3TnpJPQ==\"; PHPSESSID=2dea5464a9bbd8415f531ce3; _jpuid=\"OWZmNzg1MGY1YjRmZTg1Y2FmYzdhNmRmMTRhMzBkMDAjMTY1OTgwNzQyMzczNyMxNzI4MDAjTVE9PQ==\"; csrf_token=4557407768a4edba1fc2337a4dd0b4f0; Hm_lvt_bfe2407e37bbaa8dc195c5db42daf96a=1659807424; Hm_lpvt_bfe2407e37bbaa8dc195c5db42daf96a=1659808194",
        "Host": "tencent.a1000.top:8080",
        "Origin": "http://tencent.a1000.top:8080",
        "Referer": "http://tencent.a1000.top:8080/admin/article/write",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.60 Safari/537.36",
        "X-Requested-With": "XMLHttpRequest"
    }


def bodys(title2, summary, content):
    return {
        "article.status": "normal",
        "article.id": "",
        "article.user_id": "1",
        "article.edit_mode": "html",
        "article.title": title2,
        "article.slug": "",
        "article.content": content,
        "article.summary": summary,
        "article.meta_keywords": "",
        "article.meta_description": "",
        "article.order_number": "",
        "article.link_to": "",
        "article.created": "",
        "article.comment_status": "false",
        "category": "6",
        "article.thumbnail": "/attachment/20220723/7c84d24e16704268b252b113a86a332f.jpg",
        "article.style": "",
        "article.flag": "",
        "csrf_token": "4557407768a4edba1fc2337a4dd0b4f0"
    }


def download_img(title, img_url):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36'
    }
    r = requests.get(img_url, headers=headers, stream=True)
    # print(r.status_code) # 返回状态码
    fileName = ''
    if r.status_code == 200:
        if not os.path.isdir("./img/" + title):
            os.makedirs("./img/" + title)
        # 截取图片文件名
        img_name = img_url.split('#')[0].split('?')[0].split('/').pop()
        if '.png' in img_name or '.jpg' in img_name or '.jpeg' in img_name or '.gif' in img_name or '.webp' in img_name:
            fileName = "./img/" + title + "/" + img_name
            with open(fileName, 'wb') as f:
                f.write(r.content)
            print("download img", fileName)
            # 去水印
            clearWater2(fileName)
        return fileName
    else:
        return fileName


def clearWater2(imgPath):
    img = Image.open(imgPath)
    width, height = img.size
    for pos in product(range(width), range(height)):
        if sum(img.getpixel(pos)[:3]) > 600:
            img.putpixel(pos, (255, 255, 255))
    img.save(imgPath)


# 去除文件名中的特殊字符 +-*#&!~^%$
def fixname(filename):
    intab = r'[?*/\|.:><]'
    filename = re.sub(intab, "", filename)  # 用正则表达式去除windows下的特殊字符，这些字符不能用在文件名
    return filename


def upload(cookie, xtoken, title, img):
    title = re.sub('[^\w\-_\. ]', '', title)
    # print(title)
    name = download_img(title, img)
    if len(name) > 0:
        # t = time.time()
        # timestamp = int(round(t * 1000))  # 13位
        res = uploadimg(cookie, xtoken, name)
        # 'application/octet-stream'
        if res.status_code == 200:
            print('上传成功' if res.text == '' else eval("u" + "\'" + res.text + "\'"))
            return json.loads(res.text)['data']
        # {"message":"success","code":0,"data":"http://jeacher-assets.oss-cn-beijing.aliyuncs.com/blogs/1659868810227dwe5w87uzi.png","requestId":null}
        print('上传失败' if res.text == '' else eval("u" + "\'" + res.text + "\'"))
        return ""
    else:
        return ""


# status 1草稿 2发布
def putBlogs(cookie2, xtoken2, title, content, html):
    aheaders = {'Content-Type': 'application/json; charset=UTF-8',
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
                'Cookie': cookie2,
                'x-token': xtoken2,
                'Referer': 'http://jeacher.com/jeacher/blogsEditor/blogsEditor.html'}
    # blogurl = "http://47.93.17.8/content/insert/blogs"
    # blogurl = "http://47.93.17.8/content/insert/content"
    blogurl = "http://jeacher.com/content/draft/blogs"
    # id 修改指定文档用的
    adata = {
        'title': title,
        # 'content': content,
        'blogsHtml': html,
        # 'contentType': '2',
        # 'posterUrl': '',
        # 'status': '1',
        'id': '',
    }
    response = requests.post(blogurl, headers=aheaders, json=adata)
    print(response.text)


# <img src="https://www.runoob.com/wp-content/uploads/2013/12/20171102-1.png">
# <img class="imgBox" src="http://jeacher-assets.oss-cn-beijing.aliyuncs.com/blogs/165867134571340.png">
def getContentDiv(cookie, xtoken, bgurl):
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36'
    }
    res = requests.get(bgurl, headers=headers)
    cdata = res.content
    cont = BeautifulSoup(cdata, 'html.parser')
    # 获取包含文章内容的标签 attrs后跟的是最外层标签属性，根据爬取网站的实际情况进行修改 id:content  class:mainxx
    # data = cont.find('div', attrs={'class': 'article-body'}).text
    data = cont.find('div', attrs={'id': 'cnblogs_post_body'})  # 博客园
    # print(data)
    # print(tables)
    # for table in cont.select('table.reference'):  # class="reference"

    myh1 = cont.find("a", attrs={'id': 'cb_post_title_url'}).text
    title = myh1.strip()
    if len(myh1) > 5:
        if len(myh1) > 50:
            title = myh1[:49]
    else:
        dt = time.strftime('%Y年%m月%d日  %H时%M分%S秒', time.localtime())
        title = myh1 + "-" + dt

    # 表、code
    for table in data.find_all("table"):
        table["class"] = "tableBox"
    for tr in data.find_all("tr"):
        tr["class"] = "trBox"
    tds = data.find_all("td")
    for td in tds:
        td["class"] = "tdBox"
    ths = data.find_all("th")
    for th in ths:
        th["class"] = "tdBox"
    replacement = """
               <pre class="codeBox">
               {text}
               </pre>
               """
    for precode in data.find_all('pre'):
        # precode["class"] = "codeBox"
        precode.replace_with(BeautifulSoup(replacement.format(text=precode.text), 'html.parser'))

    # 图操作
    for imgv in data.find_all('img'):
        yimg = imgv["src"]
        if 'cnblogs.com/blog' in yimg:
            print(yimg)
            # title = re.sub("[^a-zA-Z0-9\u4e00-\u9fa5]", '', title)
            ossimg = upload(cookie, xtoken, title, yimg)
            if len(ossimg) > 0:
                imgv["src"] = ossimg
                imgv["class"] = "imgBox"
    print('*************************************************\n')
    data.prettify()

    # print(cont.find('div', class_="article-body"))
    value = cont.find('div', attrs={'id': 'cnblogs_post_body'})

    # exp = exportContent.Extractor(bgurl, blockSize=5, image=False)
    # beirf = exp.getContext()[:150]
    beirf = value.text.strip()[:150]
    # print(value) #去除首尾空格
    valueStr = str(value).strip()  # .replace("\\n", "").replace("\\r", "").replace("\\ss", "")
    # print(title, valueStr)
    putBlogs(cookie, xtoken, title, beirf, valueStr)


def getDiv(mdPath):
    file_object2 = open(mdPath, 'r', encoding="UTF-8")
    index = 0
    arr = []
    # rex = """(?|(?<txt>(?<url>(?:ht|f)tps?://\S+(?<=\P{P})))|\(([^)]+)\)\[(\g<url>)\])"""
    rex = "(?<=\\]\\()[^\\)]+"  # 匹配]()中间
    # rex = "(?<=\\]().*?(?=\\))"  # 匹配]()区间但不包含()
    try:
        lines = file_object2.readlines()
        print("type(lines)=", type(lines))  # type(lines)= <type 'list'>
        for line in lines:
            # print("line=", line)
            pattern = regex.compile(rex)
            matches = regex.findall(pattern, line, overlapped=True)
            for herf in matches:
                index += 1
                arr.append(herf)
                # arr.insert(index,herf)
                print(index, herf)
    finally:
        file_object2.close()
    return arr


# suffix  .png
def uploadimg(cookie, xtoken, img_path):
    url1 = 'http://jeacher.com/content/blogs/image/upload'
    # from_data上传文件，注意参数名uploadFile
    filename, suffix = os.path.splitext(img_path)
    fname = str(filename).split("/").pop() + suffix
    opf = open(img_path, 'rb')
    data = MultipartEncoder(
        fields={'suffix': suffix, 'uploadFile': (fname, opf, 'image/*')})
    # fields={'suffix': suffix, 'uploadFile': ('filename', open(img_path, 'rb'), 'image/*')})
    aheaders = {'Content-Type': data.content_type,
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36',
                'Cookie': cookie,
                'x-token': xtoken,
                'Referer': 'http://jeacher.com/jeacher/blogsEditor/blogsEditor.html'}
    res0 = requests.post(url=url1, data=data, headers=aheaders)
    opf.close()
    return res0

cookie0 = 'phone=10000000000; token=930c7c291ade1753df2c7ba125e339ba4f3fc51fe2a0527bfc20b4976aac770085239b4936cfa8c11aec78479e6d7b485fbe6e5dd9dd4c2c661cf58ffb909ebf8cffc07dbd275548c776b80139152fc596110b5c502e2aab15fbc2e9160e95d0fa0ebc64ec28ca41bc5be57954d31f3f46e272d93bfe9bfe590b2ee888b18799e6a80ff21a2b9c4a932b484190edb62b698d505a39e275ba3aeae55b43a6309200fb2d3947c147c975e99c1cf47554ead69da67182caa521adfa304537d7f258c46a748d8c83647bd7d89f3cd791b69a3614a1d694036da7a7f7b54dcd66c37865f64a18648f4d9f411629158d94cdc87ded01c0ef154529e94b197e881f47d4a9ecf8fa20428cbeb6cc67204678c7cd8fc0d19d8548c22e6b4fa2af34e8cd1f; test=aaaa'
xtoken0 = '930c7c291ade1753df2c7ba125e339ba4f3fc51fe2a0527bfc20b4976aac770085239b4936cfa8c11aec78479e6d7b485fbe6e5dd9dd4c2c661cf58ffb909ebf8cffc07dbd275548c776b80139152fc596110b5c502e2aab15fbc2e9160e95d0fa0ebc64ec28ca41bc5be57954d31f3f46e272d93bfe9bfe590b2ee888b18799e6a80ff21a2b9c4a932b484190edb62b698d505a39e275ba3aeae55b43a6309200fb2d3947c147c975e99c1cf47554ead69da67182caa521adfa304537d7f258c46a748d8c83647bd7d89f3cd791b69a3614a1d694036da7a7f7b54dcd66c37865f64a18648f4d9f411629158d94cdc87ded01c0ef154529e94b197e881f47d4a9ecf8fa20428cbeb6cc67204678c7cd8fc0d19d8548c22e6b4fa2af34e8cd1f'
cookie1 = 'test=aaaa; token=619f3ccce41c10d2bc1dac9bc43b9bab674fc986e1d315adc56f466b53180dee26ace56889787eff59dc5fcdf2376a4513edb47bd60f335d76186f8a1b52d44ef0d79405d61cb43add9a71b44cba5256e2afc3e62bc25c18ffcb50c4aa82527ae77a9c90a2ca3268542ff3e9ac11f3c97180bb0ef82e6058cd1964dee217a3375bc6657338d126050a0489ec6ea307abaa715f472ad12587533ae70e37a8036b98d3fa4fdf53c1225e2071c8d02459032f198c2476b929f82ac514194e090c3658afafc2762918a10ba4a45f9cb1601884e08e29ff2cd33a273f4a550cbf2b8ba6ca72efc79ab973ed4b4f02334392eb1be8bc66e7167591b79c678d849f1313cd3af6d7aff4da65976fbe4fab790b7e02b921427886a19b2137f91cc3119e1c666cd0c0f94feb3a2c35a7d68f41eb5d; phone=10000000001'
xtoken1 = '619f3ccce41c10d2bc1dac9bc43b9bab674fc986e1d315adc56f466b53180dee26ace56889787eff59dc5fcdf2376a4513edb47bd60f335d76186f8a1b52d44ef0d79405d61cb43add9a71b44cba5256e2afc3e62bc25c18ffcb50c4aa82527ae77a9c90a2ca3268542ff3e9ac11f3c97180bb0ef82e6058cd1964dee217a3375bc6657338d126050a0489ec6ea307abaa715f472ad12587533ae70e37a8036b98d3fa4fdf53c1225e2071c8d02459032f198c2476b929f82ac514194e090c3658afafc2762918a10ba4a45f9cb1601884e08e29ff2cd33a273f4a550cbf2b8ba6ca72efc79ab973ed4b4f02334392eb1be8bc66e7167591b79c678d849f1313cd3af6d7aff4da65976fbe4fab790b7e02b921427886a19b2137f91cc3119e1c666cd0c0f94feb3a2c35a7d68f41eb5d'
cookie2 = 'test=aaaa; token=619f3ccce41c10d2bc1dac9bc43b9bab674fc986e1d315adc56f466b53180dee26ace56889787eff59dc5fcdf2376a4513edb47bd60f335d76186f8a1b52d44ef0d79405d61cb43add9a71b44cba5256e2afc3e62bc25c18ffcb50c4aa82527abb9ea5c1d81350fadf3777973e8850bbd88d526df033a05f366be4903c53eb715bc6657338d126050a0489ec6ea307abab609e316e9d2b740e98af900f8d728398d3fa4fdf53c1225e2071c8d0245903c6d0baba0da10d15233b3dec24ff6baf27dace63bb355b2e3815676a735a3baf49d6379e7b91f88038e1831fd7f1e47ed5d46a433b82ae2d7d9c3389730704c6837ad6150108096c30a6cf0eb359413a7da74d1b3c88f2732619f429a27ce698340f114419d0b89b3a73324d8409cb4a3769f287f3ae00939f96fc78396b9e012a26eb4b48ea69ae5e192275d2123a11071d9443e559c21745a8ff8f5c6cd969fbc497b5a3dcb7130647c61e487d94c9; phone=10000000002'
xtoken2 = '619f3ccce41c10d2bc1dac9bc43b9bab674fc986e1d315adc56f466b53180dee26ace56889787eff59dc5fcdf2376a4513edb47bd60f335d76186f8a1b52d44ef0d79405d61cb43add9a71b44cba5256e2afc3e62bc25c18ffcb50c4aa82527abb9ea5c1d81350fadf3777973e8850bbd88d526df033a05f366be4903c53eb715bc6657338d126050a0489ec6ea307abab609e316e9d2b740e98af900f8d728398d3fa4fdf53c1225e2071c8d0245903c6d0baba0da10d15233b3dec24ff6baf27dace63bb355b2e3815676a735a3baf49d6379e7b91f88038e1831fd7f1e47ed5d46a433b82ae2d7d9c3389730704c6837ad6150108096c30a6cf0eb359413a7da74d1b3c88f2732619f429a27ce698340f114419d0b89b3a73324d8409cb4a3769f287f3ae00939f96fc78396b9e012a26eb4b48ea69ae5e192275d2123a11071d9443e559c21745a8ff8f5c6cd969fbc497b5a3dcb7130647c61e487d94c9'

if __name__ == "__main__2":
    urls = getDiv("../cnblog/md/mahongyin-blog-list.md")
    # urls = ['https://www.cnblogs.com/stars-one/p/15270435.html']
    for url in urls:
        # print(url)
        getContentDiv(cookie2, xtoken2, url)
        time.sleep(1)
if __name__ == '__main__':
    # upload(cookie0, xtoken0, "测试",
    #        "https://img2020.cnblogs.com/blog/1210268/202110/1210268-20211004164242856-179706561.png")
    getContentDiv(cookie2,xtoken2,"https://www.cnblogs.com/sheepboy/p/16082594.html")

# coding=utf-8

import datetime
import os
import time

from flask import Flask, request

app = Flask(__name__)


# 图片提取文字功能

def get_time_stamp():
    times = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    array = time.strptime(times, "%Y-%m-%d %H:%M:%S")
    time_stamp = int(time.mktime(array))
    return time_stamp


@app.route('/image/extract', methods=['POST'])
def pure_rec():
    global plist
    file = request.files.get('file')
    ts = file.filename
    up_path = os.path.join(ts)
    file.save(up_path)
    cmd = "tesseract " + up_path + " " + ts + " -l chi_sim"
    print("cmd:" + cmd)
    result = os.system(cmd)
    if result == 0:
        plist = ""
        for line in open(ts+".txt", encoding='utf-8'):
            plist += line
    return "{\"code\":0,\"data\":\"" + plist + "\"}"


if __name__ == '__main__':
    app.run(debug=True)

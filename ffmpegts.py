import os
import logging
from ffmpy import FFmpeg
import random
import string
import json
# 使用ffmpeg对指定格式的视频进行切片 并输出ts和m3u8文件

logger = logging.getLogger(__name__)
class Trans(object):

    def __init__(self):
        # self.base = r'E:\output_movie'
        self.base = os.getcwd() + r"\download"

    # 手动创建文件夹'E:\sywj'  用来存放m3u8文件
    # 找出下面带mp4的文件 输出路径  类似这种 E:\种子\URVRSP-052\URVRSP-052-C.mp4
    def findAllFile(self, base):
        for root, ds, fs in os.walk(self.base):
            for f in fs:
                if f.endswith('.mp4'):
                    fullname = os.path.join(root, f)
                    print(fullname)
                    yield fullname

    # 遍历出base下面的文件 返回一条视频路径
    def video_path_input(self):

        for video_path in self.findAllFile(self.base):
            video_path = video_path.replace('\\', '/')
            yield video_path

    # 首先mp4格式的电影全部转成h264编码格式的电影
    def trans_video(self, video_path):
        ff = FFmpeg(
            inputs={video_path: None},
            outputs={video_path: '-strict -2 -vcode h264'}
        )
        print(ff.cmd)
        ff.run

    # mp4格式的电影 复制成一个大的ts文件
    def cpts_video(self, video_path):
        ts_path = video_path.replace('.mp4', '.ts')
        ff = FFmpeg(
            inputs={video_path: None},
            outputs={ts_path: '-c copy'}
        )
        print(ff.cmd)
        try:
            ff.run()

        except Exception as e:
            pass
        if os.path.exists(video_path):  # 如果文件存在
            # 删除文件，可使用以下两种方法。
            os.remove(video_path)
        return ts_path

    def get_name(self):
        # 随机输出8位由英文字符和数字组成的字符串 当m3u8名字
        salt = ''.join(random.sample(string.ascii_letters + string.digits, 8))
        return salt

    # 视频切片 输出索引和ts小文件 返回索引名称和需要切片的ts路径
    def cut_tsvideo(self, salt, ts_path):

        # 随机输出8位由英文字符和数字组成的字符串 当m3u8名字
        # salt = self.get_name()
        salt_ts = ''.join(random.sample(string.ascii_letters + string.digits, 8))
        p, f = os.path.split(ts_path)
        m3_path = p + '/'

        ff = FFmpeg(
            inputs={ts_path: None},
            outputs={
                # None: '-c copy -map 0 -y -f segment -segment_list {0} -segment_time 6  -bsf:v h264_mp4toannexb  {1}{2}%03d.ts'.format(
                #     r'E:\\sywj\\{0}.m3u8', m3_path, salt_ts).format(salt)
                None: '-c copy -map 0 -y -f segment -segment_list {0} -segment_time 6  -bsf:v h264_mp4toannexb  {1}{2}%03d.ts'.format(
                    os.getcwd() + r'\\download\\{0}.m3u8', m3_path, salt_ts).format(salt)
            }
        )
        print(ff.cmd)
        try:
            ff.run()

        except Exception as e:
            pass
        return salt, ts_path

    # 输出60秒时候的截图 作为封面
    def output_img(self, salt, ts_path):

        # out_path_img = r'E:\\img\\{}.jpg'.format(salt)
        out_path_img = os.getcwd() + r'\download\\{}.jpg'.format(salt)
        ff = FFmpeg(
            inputs={ts_path: None},
            outputs={
                out_path_img: '-y -f mjpeg -ss 60 -f image2',

            }
        )
        print(ff.cmd)
        try:
            ff.run()

        except Exception as e:
            pass

        if os.path.exists(ts_path):  # 如果文件存在
            # 删除文件，可使用以下两种方法。
            os.remove(ts_path)

    # 写入文件 相对路径 对应的索引文件名
    def write_relations(self, salt, ts_path):

        rout = ts_path.split('/')[-2]

        data = {
            'rote': '/{}/'.format(rout),
            'm3u8_name': '{}.m3u8'.format(salt)
        }

        content = json.dumps(data, ensure_ascii=False)
        fp = open(os.getcwd() + r'\download\movie.txt', 'a')
        fp.write(content + '\n')
        fp.close()


if __name__ == '__main__':
    go = Trans()
    for i in go.video_path_input():
        print(i + '切片开始----------------------------------------------------------------------')

        go.trans_video(i)
        ts_path = go.cpts_video(i)
        salt = go.get_name()
        go.cut_tsvideo(salt, ts_path)
        go.output_img(salt, ts_path)
        go.write_relations(salt, ts_path)
        print(i + '切片完成——————————————————————————————————————————————————————————————————————————————')
